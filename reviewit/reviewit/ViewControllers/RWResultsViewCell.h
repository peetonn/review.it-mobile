//
//  RWResultsViewCell.h
//  reviewit
//
//  Created by Peter Gusev on 8/6/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

/**
 Represents a cell in the list of results shown by RWListResultsViewController.
 */

#import <UIKit/UIKit.h>
#import "RWBusinessBrief.h"

@interface RWResultsViewCell : UITableViewCell
{
    __weak RWBusinessBrief *_business;
}

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UILabel *categoryLabel;
@property (nonatomic, strong) IBOutlet UILabel *reviewsNumLabel;
@property (nonatomic, strong) IBOutlet UILabel *descriptionLabel;

@property (nonatomic, strong) IBOutlet UIImageView *logo;
@property (nonatomic, strong) IBOutlet UIImageView *categoryIcon;

@property (nonatomic, weak) RWBusinessBrief *business;

@end
