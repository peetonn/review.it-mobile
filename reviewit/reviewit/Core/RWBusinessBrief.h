//
//  RWBusinessBrief.h
//  reviewit
//
//  Created by Peter Gusev on 7/25/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RWBaseObject.h"
#import <CoreLocation/CoreLocation.h>
#import "RWIndustry.h"
#import "RWBusinessDetails.h"
#import "RWLocation.h"
#import <MapKit/MapKit.h>

/**
 * RWBusinessBrief represents brief information about business shown in search results list.
 */
@interface RWBusinessBrief : RWBaseObject<MKAnnotation>
{
    __strong UIImage *_logo;
}
/**
 * @name Properties
 */
/**
 * Name of the business
 */
@property (nonatomic, retain ) NSString *name;
/**
 * Category of business
 */
@property (nonatomic, retain) RWIndustry *category;
/**
 * Details of business
 */
@property (nonatomic, retain) RWBusinessDetails *details;
/**
 * Geographical coordinates of the business - MKAnnotation protocol
 */
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
/**
 * Geographical location of the business
 */
@property (nonatomic, readonly) CLLocation *location;
/**
 * Description of business
 */
@property (nonatomic, retain) NSString *description;
/**
 * Total number of reviews
 */
@property (nonatomic) NSUInteger reviewsNum;
/**
 * Website
 */
@property (nonatomic, retain) NSString *website;
/**
 * Business' logo
 */
@property (nonatomic, readonly) UIImage *logo;
/**
 * Returns YES if business has logo
 */
@property (nonatomic, readonly) BOOL hasLogo;
/**
 * Determines whether location of business can be geocoded
 */
@property (nonatomic, readonly) BOOL canGeocode;
/**
 * Determines wif geocoding needed
 */
@property (nonatomic, readonly) BOOL isGeocodingNeeded;

/**
 * MKMapAnnotation protocol conformance
 */
/**
 * Title of the map annotation - name of the business
 */
@property (nonatomic, readonly, copy) NSString *title;
/**
 * Subtitle of the map annotation - address
 */
@property (nonatomic, readonly, copy) NSString *subtitle;

/**
 * @name Class methods
 */
/**
 * Load business briefs for specific keyword in specific location with parameters
 * @param keyword Keyword for search
 * @param location Location for search algorithm
 * @param limit Number of results to load in one request
 * @param page Number of page to load
 * @param block Callback to be called upon receiving results
 */
+(void)businessBriefsForKeyword:(NSString*)keyword
                        location:(RWLocation*)location
                          limit:(NSUInteger)limit
                         pageNo:(NSUInteger)page
                completionBlock:(RWObjectArrayLoadingCallback)block;

/**
 * Workaround for location-based search
 */
+(void)businessBriefsForKeyword:(NSString*)keyword
                      nearLocation:(CLLocation*)location
                        withRadius:(float)metersRadius
                   completionBlock:(RWObjectArrayLoadingCallback)block;
/**
 * @name Instance methods
 */
/**
 * Load business' image
 */
-(void)loadImageInBlock:(RWImageLoadingCallback)block;
/**
 * Starts geocoding
 */
-(void)geocodeLocationCallback:(RWObjectLoadingCallback)block;
/**
 * Checks, whether business is lying near specified location defined by radius
 */
-(BOOL)isLyingNearLocation:(CLLocation*)location withinDistance:(float)radius;


@end
