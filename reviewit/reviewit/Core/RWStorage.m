//
//  RWStorage.m
//  reviewit
//
//  Created by Peter Gusev on 7/26/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "RWStorage.h"
#import <OHAttributedLabel/NSAttributedString+Attributes.h>
#import "UIColor+RWThemeColors.h"

#define RW_PLISTKEY_BASEURL @"Base URL"
#define RW_PLISTKEY_API_VERSION @"Current API version"
#define RW_PLISTKEY_QUICK_SEARCH_PATH @"Quick search resource path"
#define RW_PLISTKEY_SAVED_LOCATION @"Last location"
#define RW_PLISTKEY_RECENT_LOCATIONS_ARRAY @"Recent locations"
#define RW_PLISTKEY_CLIENT_TYPE @"Client type"
#define RW_PLISTKEY_REVIEW_PATH @"Reviews resource path"
#define RW_PLISTKEY_LOADING_REVIEW_LIMIT @"Loading reviews limit"
#define RW_PLISTKEY_PROFILE_URL_FORMAT @"Profile image URL"
#define RW_PLISTKEY_FB_COMMENTS_PLUGIN_URL_FORMAT @"Comments page URL format"
#define RW_PLISTKEY_BUSINESS_LOGO_URL_FORMAT @"Business image URL format"
#define RW_PLISTKEY_BUBBLE_TEXT @"Bubble text"
#define RW_PLISTKEY_BUBBLE_BOLD_TEXT @"Bubble bold text"
#define RW_PLISTKEY_BUBBLE_LINK @"Bubble link"
#define RW_PLISTKEY_RESULTS_LIMIT @"Search results limit"
#define RW_PLISTKEY_LOCATION_SEARCH_RADIUS @"Location search radius"

@interface RWStorage ()

@end

@implementation RWStorage

+(RWStorage*)sharedStorageController
{
    return [[RWStorage alloc] initWithStorageFile:RW_SETTINGS_FILE];
}

-(NSString*)getBaseURLStringForCurrentAPI
{
    NSString *baseURL = [self getParamWithName:RW_PLISTKEY_BASEURL];
    NSString *APIversion = [self getParamWithName:RW_PLISTKEY_API_VERSION];
    
    return [baseURL stringByAppendingFormat:@"/%@",APIversion];
}

-(NSString*)getCurrentAPIVersion
{
    return [self getParamWithName:RW_PLISTKEY_API_VERSION];
}

-(void)setCurrentAPIVersion:(NSString*)apiVersion
{
    [self saveParam:apiVersion forKey:RW_PLISTKEY_API_VERSION];
}

-(NSString*)getQuickSearchResourcePathString
{
    return [self getParamWithName:RW_PLISTKEY_QUICK_SEARCH_PATH];
}

-(void)setLocation:(RWLocation*)location
{
    [self saveParam:location.dictionary forKey:RW_PLISTKEY_SAVED_LOCATION];
}

-(RWLocation*)getSavedLocation
{
    return [RWLocation locationFromDictionary:[self getParamWithName:RW_PLISTKEY_SAVED_LOCATION]];
}    

-(NSArray*)getRecentLocations
{
    NSArray *dictArray = [self getParamWithName:RW_PLISTKEY_RECENT_LOCATIONS_ARRAY];
    NSMutableArray *locations = [NSMutableArray arrayWithCapacity:[dictArray count]];
    
    [dictArray enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL *stop){
        [locations addObject:[RWLocation locationFromDictionary:dict]];
    }];
    
    return locations;
}

-(void)addRecentLocation:(RWLocation *)location
{
    NSMutableArray *recentLocations = [NSMutableArray arrayWithArray:[self getRecentLocations]];
    
    if ([recentLocations containsObject:location])
        return;
    
    [recentLocations insertObject:location atIndex:0];
    
    while ([recentLocations count] > RWRecentLocationsNum)
        [recentLocations removeObjectAtIndex:RWRecentLocationsNum];
    
    NSMutableArray *dictArray = [NSMutableArray arrayWithCapacity:[recentLocations count]];
    
    [recentLocations enumerateObjectsUsingBlock:^(RWLocation *location, NSUInteger idx, BOOL *stop){
        [dictArray addObject:location.dictionary];
    }];
    
    [self saveParam:dictArray forKey:RW_PLISTKEY_RECENT_LOCATIONS_ARRAY];
}

-(NSString*)getCurrentClient
{
    return [self getParamWithName:RW_PLISTKEY_CLIENT_TYPE];
}

-(NSString*)getReviewsResourcePathString
{
    return [self getParamWithName:RW_PLISTKEY_REVIEW_PATH];
}

-(NSUInteger)getLoadingReviewsLimit
{
    return [[self getParamWithName:RW_PLISTKEY_LOADING_REVIEW_LIMIT] intValue];
}

-(NSString*)getProfileURLStringForUserID:(NSString*)userID
{
    return [NSString stringWithFormat:[self getParamWithName:RW_PLISTKEY_PROFILE_URL_FORMAT], userID];
}

-(NSURL*)getFBCommentsURLForBusinessID:(NSUInteger)businessID
{
    return [NSURL URLWithString:[NSString stringWithFormat:[self getParamWithName:RW_PLISTKEY_FB_COMMENTS_PLUGIN_URL_FORMAT], [NSNumber numberWithInt: businessID]]];
}

-(NSString*)getBusinessLogoURLStringWithName:(NSString*)logoName
{
    return [NSString stringWithFormat:[self getParamWithName:RW_PLISTKEY_BUSINESS_LOGO_URL_FORMAT], logoName];
}

-(NSAttributedString*)getBubblePromoString
{
    NSString *bubbleText = [self getParamWithName:RW_PLISTKEY_BUBBLE_TEXT];
    NSMutableAttributedString *bubbleString = [NSMutableAttributedString attributedStringWithString:bubbleText];
    
    [bubbleString setFont:[UIFont systemFontOfSize:14]];
    [bubbleString setTextColor: [UIColor rwWhiteThemeColor]];
    [bubbleString setTextBold:YES range:[bubbleText rangeOfString:[self getBubbleLinkText]]];
    
    return bubbleString;
}

-(NSString*)getBubbleLinkText
{
    return [self getParamWithName:RW_PLISTKEY_BUBBLE_BOLD_TEXT];
}

-(NSURL*)getBubbleURL
{
    return [NSURL URLWithString:[self getParamWithName:RW_PLISTKEY_BUBBLE_LINK]];
}

-(NSUInteger)resultsLimit
{
    return [[self getParamWithName:RW_PLISTKEY_RESULTS_LIMIT] intValue];
}

-(float)locationSearchRadius
{
    return [[self getParamWithName:RW_PLISTKEY_LOCATION_SEARCH_RADIUS] floatValue];
}
@end
