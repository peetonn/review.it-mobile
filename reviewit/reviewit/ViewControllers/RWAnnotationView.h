//
//  RWAnnotationView.h
//  reviewit
//
//  Created by Peter Gusev on 8/22/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

/**
 Provides the marker on the map provided by RWMapResultsViewController, which the user can tap for more info.
 */

#import <MapKit/MapKit.h>

@interface RWAnnotationView : MKAnnotationView
{
    UIImage *_highlightedImage, *_normalImage;
}

@end
