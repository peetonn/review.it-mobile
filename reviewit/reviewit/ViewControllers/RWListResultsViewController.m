//
//  RWListResultsViewController.m
//  reviewit
//
//  Created by Peter Gusev on 7/17/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "RWListResultsViewController.h"
#import "RWBusinessBrief.h"
#import "RWResultsViewController.h"
#import "RWProfileViewController.h"
#import <PTNArtifacts/PTNLogger.h>
#import "RWConstants.h"
#import "RWFunctionCell.h"
#import <PTNArtifacts/PTNArtifacts.h>

#define RW_RESULTS_CELL_DEF_HEIGHT 105.

@interface RWListResultsViewController ()
@property (nonatomic) RWSearchResult searchResultState;
@property (nonatomic, readonly) RWResultsInformationViewController *informationViewController;
@end

@implementation RWListResultsViewController
@synthesize informationViewController = _informationViewController, searchResultState = _searchResultState;

//********************************************************************
#pragma mark - received actions
// <#your IBAction's handlers goes here#>

//********************************************************************
#pragma mark - initialization and memory management

//********************************************************************
#pragma mark - view lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.rowHeight = 105.;
    // load information view controller
    [self informationViewController];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"OpenProfileList"])
    {
        RWProfileViewController *profileVC = segue.destinationViewController;
        profileVC.business = [self.searchResults objectAtIndex:[self.tableView indexPathForSelectedRow].row];
    }
}
//********************************************************************
#pragma mark - delegation: RWInformationViewControllerDelegate
-(void)resultsInformationViewControllerDidTappedTryAgainLink:(RWResultsInformationViewController *)vc
{
    [self initiateSearch];
}

//********************************************************************
#pragma mark - protocol: RWSearchResultsContentProtocol
-(void)initiateSearch
{
    self.currentLoadedPage = 0;
    self.lastRequestEmpty = NO;
    self.searchResultState = RWSearchResultOK;
    self.searchResultsDataController.searchResults = [NSArray array];
    [self.tableView reloadData];
}
-(void)didCurrentSearchResultsStateChanged:(RWSearchResult)newResult
{
    if (newResult == RWSearchResultOK)
    {
        self.currentLoadedPage = 0;
        self.lastRequestEmpty = NO;
        self.searchResultState = RWSearchResultOK;
        [self.tableView reloadData];
    }
    else
        self.searchResultState = newResult;
}
-(id<RWSearchResultsDataController>)searchResultsDataController
{
    return (id<RWSearchResultsDataController>)self.parentViewController;
}

#pragma mark - Table view data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = [self.searchResults count] + [super tableView:tableView numberOfRowsInSection:section];
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger functionalCellRowIdx = [self shouldAddFunctionalCell]?self.searchResults.count:-1;
    
    if (indexPath.row == functionalCellRowIdx) // return "loading" cell
    {
        [self loadNextResultsPage];
        
        RWFunctionCell *cell = (RWFunctionCell*)[tableView dequeueReusableCellWithIdentifier:@"RWFunctionCell"];
        [cell configureViewForPath:indexPath];
        
        return cell;
    }
    else // return "business" cell
    {
        static NSString *CellIdentifier = @"RWResultsViewCell";
        RWResultsViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        RWBusinessBrief *business = [self.searchResults objectAtIndex:indexPath.row];
        
        cell.business = business;
        
        return  cell;
    }

    return nil;
}

#pragma mark - Table view delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger functionalCellIdx = [self shouldAddFunctionalCell]?self.searchResults.count:-1;
    
    if (indexPath.row == functionalCellIdx)
        return RW_FUNCTIONALCELL_DEF_HEIGHT;
    else
        return RW_RESULTS_CELL_DEF_HEIGHT;
}

//********************************************************************
#pragma mark - notifications
// <#your notification's handlers' code goes here#>

//********************************************************************
#pragma mark - overriden
-(BOOL)hasMorePages
{
    return (self.searchResultState == RWSearchResultOK || self.searchResultState == RWSearchResultLoading) && [super hasMorePages];
}
//********************************************************************
#pragma mark - class methods
// <#your class methods code goes here#>

//********************************************************************
#pragma mark - properties
-(void)setSearchResultState:(RWSearchResult)newState
{
    _searchResultState = newState;
    [self setInfoViewVisible:(newState == RWSearchResultEmpty || newState == RWSearchResultError) state:newState];
}
-(NSArray*)searchResults
{
    return self.searchResultsDataController.searchResults;
}
-(RWResultsInformationViewController*)informationViewController
{
    if (!_informationViewController)
    {
        _informationViewController = [[RWResultsInformationViewController alloc] init];
        [[NSBundle mainBundle] loadNibNamed:@"RWResultsInformationView" owner:_informationViewController options:nil];
        _informationViewController.delegate = self;
    }
    return _informationViewController;
}

//********************************************************************
#pragma mark - public methods
// <#your public methods' code goes here#>

//********************************************************************
#pragma mark - private methods
-(void)setInfoViewVisible:(BOOL)setVisible state:(RWSearchResult)state
{
    [self.informationViewController setStateForSearchResultState:state];
    [self.view viewOnTop:self.informationViewController.view setSisible:setVisible];
}

-(void)loadNextResultsPage
{
    if (self.isLoadingNextPage)
        return;
    
    self.isLoadingNextPage = YES;
    self.searchResultState = RWSearchResultLoading;
    [self.searchResultsDataController searchResultsContentViewControllerWillStartSearch:self];
    [RWBusinessBrief businessBriefsForKeyword:self.searchResultsDataController.searchKeyword
                                     location:self.searchResultsDataController.searchLocation
                                        limit:[[RWStorage sharedStorageController] resultsLimit]
                                       pageNo:self.currentLoadedPage+1
                              completionBlock:^(NSArray *objArray, NSError *err){
                                  [self.searchResultsDataController searchResultsContentViewControllerDidFinishSearch:self];
                                  self.isLoadingNextPage = NO;
                                  self.lastRequestEmpty = (!err && ![objArray count]);
                                  
                                  if (err && ![self.searchResults count])
                                      self.searchResultState = RWSearchResultError;
                                  else
                                      if (![self.searchResults count] && ![objArray count])
                                          self.searchResultState = RWSearchResultEmpty;
                                      else
                                          self.searchResultState = RWSearchResultOK;
                                  
                                  if (!err && [objArray count])
                                  {
                                      self.currentLoadedPage++;
                                      [self.searchResultsDataController searchResultsContentViewController:self
                                                                                           providedMoreResults:objArray
                                                                                          withSearchResult:self.searchResultState];
                                  }
                                  else if (self.searchResultState == RWSearchResultEmpty || self.searchResultState == RWSearchResultError)
                                      [self.searchResultsDataController searchResultsContentViewController:self
                                                                                       providedNewResults:nil
                                                                                          withSearchResult:self.searchResultState];
                                  
                                  [self.tableView reloadData];
                              }];
}

@end
