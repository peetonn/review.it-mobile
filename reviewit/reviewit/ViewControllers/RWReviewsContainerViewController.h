//
//  RWReviewsContainerViewController.h
//  reviewit
//
//  Created by Peter Gusev on 11/25/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "RWSuperViewController.h"
#import "RWContainerViewController.h"
#import "RWReviewsViewController.h"
#import "RWResultsInformationViewController.h"

/**
 * This class represents reivews' container view controller which operates with RWReviewsViewController and RWResultsInformationViewController.
 */
@interface RWReviewsContainerViewController : RWContainerViewController
<RWReviewsViewControllerDelegate, RWResultsInformationViewControllerDelegate>
{
    RWReviewsViewController *_reviewsViewController;
    RWResultsInformationViewController *_informationViewController;
}

/**
 * @name Instance properties
 */
/**
 * Information view controller which is used for displaying loading progress and errors notificaitons
 */
@property (weak, nonatomic, readonly) RWResultsInformationViewController *informationViewController;
/**
 * Reviews view controller used for displaying facebook comments plugin
 */
@property (weak, nonatomic, readonly) IBOutlet RWReviewsViewController *reviewViewController;
/**
 * Business concerned
 */
@property (weak, nonatomic) RWBusinessBrief *business;

/**
 * @name Received actions
 */
/**
 * Logout button tap
 */
-(IBAction)logoutTapped:(id)sender;

@end
