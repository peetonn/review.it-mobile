//
//  RWMapResultsViewController.h
//  reviewit
//
//  Created by Peter Gusev on 8/1/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

/**
 Shows the results that we get back from the API, on a map instead of the 
 alternative list created by RWListResultsViewController.
 */

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "RWMapInfoView.h"
#import "RWResultsControllerProtocols.h"

@interface RWMapResultsViewController : UIViewController
<MKMapViewDelegate, RWSearchResultsContentProtocol>

@property (strong, nonatomic) IBOutlet UIView *searchButtonView;
@property (strong, nonatomic) IBOutlet RWMapInfoView *infoView;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

-(IBAction)searchNewAreaButtonTapped:(id)sender;

@end
