//
//  RWProfileViewController.m
//  reviewit
//
//  Created by Peter Gusev on 7/13/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "RWProfileViewController.h"
#import "RWConstants.h"
#import "AppDelegate.h"
#import "RWProfileCell.h"
#import "RWComment.h"
#import "RWCommentCell.h"
#import "RWFunctionCell.h"
#import "RWReviewsContainerViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "RWDesignController.h"

#define RW_MAPS_APP_LINK_COORDINATE_FORMAT @"http://maps.google.com/maps?q=%f,%f"
#define RW_MAPS_APP_LINK_QUERY_FORMAT @"http://maps.google.com/maps?q=%@"
#define RW_PHONE_APP_LINK_FORMAT @"tel://%@"
#define RW_PROFILECELL_DEF_HEIGHT 395.

@interface RWProfileViewController ()
// sometimes number of reviews in business structure and number factual reviews
// are different (less by 1) this flag is used for checking last reviews request
//@property (nonatomic) BOOL lastRequestEmpty;
//@property (nonatomic) BOOL isLoadingReviews;
//@property (nonatomic) NSUInteger loadedReviewPage;

@property (nonatomic) CGFloat profileViewCellHeight;
@property (nonatomic, retain) NSMutableArray *reviews;
@end

@implementation RWProfileViewController

//********************************************************************
#pragma mark - received actions

//********************************************************************
#pragma mark - initialization and memory management

//********************************************************************
#pragma mark - view lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];

    self.navigationItem.title = self.business.name;
    self.navigationItem.backBarButtonItem = [RWDesignController defaultBackButtonWithTarget:self.navigationController action:@selector(popViewControllerAnimated:)];
    
    self.tableView.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background.png"]];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [RWDesignController setNavigationBar:self.navigationController.navigationBar styleShadowed:YES];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowReviews"])
    {
        RWReviewsContainerViewController *vc = (RWReviewsContainerViewController*)segue.destinationViewController;
        vc.business = self.business;
    }
}
//********************************************************************
#pragma mark - delegation: RWProfileViewDelegate
-(void)profileViewBusinessWebsitePressed:(RWProfileView *)profileView
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.business.website]];
}
-(void)profileViewBusinessAddressPressed:(RWProfileView *)profileView
{
    // check iOS 6 maps availability
    Class  mapItemClass = [MKMapItem class];
    if (mapItemClass && [mapItemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)])
    {
        // check, if coordinates are available
        if (self.business.details.latitude && self.business.details.longitude)
        {
            MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate: self.business.coordinate
                                                           addressDictionary:nil];
            [self openPlacemarkInMapsApplication:placemark forBusiness:self.business];
        }
        else // use geocoder to get location from address
        {
            CLGeocoder *geocoder = [[CLGeocoder alloc] init];
            [geocoder geocodeAddressString:self.business.details.address completionHandler:^(NSArray *placemarks, NSError *error){
                if (error)
                {
                    LOG_ERROR(@"error while geocoding business address: %@", error);                    
                }
                else
                {
                    CLPlacemark *clplacemark = [placemarks objectAtIndex:0];
                    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:clplacemark.location.coordinate addressDictionary:clplacemark.addressDictionary];

                    [self openPlacemarkInMapsApplication:placemark forBusiness:self.business];
                }
            }];
        }
    }
    else // iOS < 6.0 - use google maps instead
    {
        NSURL *mapURL = nil;
        
        // check, if coordinates are available
        if (self.business.details.latitude && self.business.details.longitude)        
            mapURL = [NSURL URLWithString:[NSString stringWithFormat:RW_MAPS_APP_LINK_COORDINATE_FORMAT,self.business.details.latitude, self.business.details.longitude]];
        else // otherwise - use address provided in "address" field
        {
            NSString *escapedAddress = [self.business.details.address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            mapURL = [NSURL URLWithString:[NSString stringWithFormat:RW_MAPS_APP_LINK_QUERY_FORMAT,escapedAddress]];
        }
        
        [[UIApplication sharedApplication] openURL:mapURL];
    }
    
}
-(void)profileViewBusinessPhonePressed:(RWProfileView *)profileView
{
    NSString *phoneNumber = [self.business.details.phoneNumber stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSURL *phoneAppURL = [NSURL URLWithString:[NSString stringWithFormat:RW_PHONE_APP_LINK_FORMAT, phoneNumber]];
    [[UIApplication sharedApplication] openURL:phoneAppURL];
}
-(void)profileViewPostReviewPressed:(RWProfileView *)profileView
{
}
-(void)profileView:(RWProfileView *)profileView descriptionExpandedWithAnimationDuration:(NSTimeInterval)duration forNewSize:(CGSize)newSize
{
    [self.tableView beginUpdates];
    self.profileViewCellHeight = newSize.height;
    [self.tableView endUpdates];
}

//********************************************************************
#pragma mark - delegation: RWCommentCell
-(void)commentCell:(RWCommentCell *)cell willChangeSize:(CGSize)newSize
{
    [self.tableView beginUpdates];
    cell.review.wasViewed = YES;
    cell.review.viewHeight = newSize.height;
    [self.tableView endUpdates];

}
-(void)commentCellDidChangeSize:(RWCommentCell *)cell
{
}

//********************************************************************
#pragma mark - delegation: Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1+self.reviews.count+[super tableView:tableView numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSInteger functionalCellRowIdx = [self shouldAddFunctionalCell]?1+self.reviews.count:-1;
    
    switch (indexPath.row) {
        case 0: // return profile info cell
        {
            RWProfileCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RWProfileInfoCell"];
            
            if (cell.business != self.business)
                cell.business = self.business;
            
            return cell;
        }
        default: // return review cell or loading cell
        {
            if (indexPath.row == functionalCellRowIdx) // return "loading" cell
            {
                [self loadNextReviewPage];
                RWFunctionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RWFunctionCell"];
                [cell configureViewForPath:indexPath];
                
                return cell;
            }
            else
            {
                RWCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RWReviewCell"];
                NSUInteger reviewIdx = indexPath.row - 1;
                RWComment *review = [self.reviews objectAtIndex:reviewIdx];

                if (cell.review != review)
                    cell.review = review;
            
                return cell;
            }
        }
    }
    
    return nil;
}

#pragma mark - Table view delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger functionalCell = [self shouldAddFunctionalCell]?1+self.reviews.count:-1;
    
    switch ([indexPath row]) {
        case 0:{ // business profile cell
            return self.profileViewCellHeight;
        }
        default: // review cell
            if (indexPath.row == functionalCell)
                return RW_FUNCTIONALCELL_DEF_HEIGHT;

            RWComment *review = [self.reviews objectAtIndex:indexPath.row-1];
            
            return (review.wasViewed)?review.viewHeight:RW_REVIEW_MIN_HEIGHT;
//            return RW_REVIEWCELL_DEF_HEIGHT;
    }
}
//********************************************************************
#pragma mark - protocol: <#protocol name#>
// <#your conformed protocol code goes here#>

//********************************************************************
#pragma mark - notifications
// <#your notification's handlers' code goes here#>

//********************************************************************
#pragma mark - overriden
-(void)initialize
{
    [super initialize];
    
    self.profileViewCellHeight = RW_PROFILECELL_DEF_HEIGHT;
    self.reviews = [[NSMutableArray alloc] init];
}
-(BOOL)hasMorePages
{
    return (self.reviews.count < self.business.reviewsNum) && [super hasMorePages];
}

//********************************************************************
#pragma mark - class methods
// <#your class methods code goes here#>

//********************************************************************
#pragma mark - properties
-(void)setBusiness:(RWBusinessBrief *)business
{
    _business = business;
}
-(RWBusinessBrief*)business
{
    return _business;
}

//********************************************************************
#pragma mark - public methods
// <#your public methods' code goes here#>

//********************************************************************
#pragma mark - private methods
-(void)openPlacemarkInMapsApplication:(MKPlacemark*)placemark forBusiness:(RWBusinessBrief*)aBusiness
{
    MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
    mapItem.name = aBusiness.name;
    mapItem.phoneNumber = aBusiness.details.phoneNumber;
    mapItem.url = [NSURL URLWithString:aBusiness.website];
    [mapItem openInMapsWithLaunchOptions:nil];
}
-(BOOL)hasReviews
{
    return self.reviews.count;
}
-(void)loadNextReviewPage
{
    if (self.isLoadingNextPage)
        return;
    
    self.isLoadingNextPage = YES;
    [RWComment reviewsForBusiness:self.business
                            limit:[[RWStorage sharedStorageController] getLoadingReviewsLimit]
                             page:++self.currentLoadedPage
                         callback:^(NSArray *reviews, NSError *error){
                             self.isLoadingNextPage = NO;
                             
                             if (!error)
                             {
                                 self.lastRequestEmpty = ([reviews count] == 0);
                                 [self.reviews addObjectsFromArray:reviews];
                             }
                             else
                             {
                                 LOG_ERROR(@"Error loading reviews: %@", error);
                                 self.currentLoadedPage--;
                             }
                             [self.tableView reloadData];
                         }];
}
@end
