//
//  RWListResultsViewController.h
//  reviewit
//
//  Created by Peter Gusev on 7/17/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

/**
 Shows the results that we get back from the API, in a UITableViewController
 using RWResultsViewCell. The alternative view is the RWMapResultsViewController.
 */

#import <UIKit/UIKit.h>
#import "RWResultsViewCell.h"
#import "RWPaginatedViewController.h"
#import "RWResultsInformationViewController.h"
#import "RWResultsControllerProtocols.h"

@interface RWListResultsViewController : RWPaginatedViewController
<RWResultsInformationViewControllerDelegate, RWSearchResultsContentProtocol>
{
    RWResultsInformationViewController *_informationViewController;
}
@end
