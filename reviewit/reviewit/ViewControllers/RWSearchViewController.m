//
//  RWSearchViewController.m
//  reviewit
//
//  Created by Peter Gusev on 7/13/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "RWSearchViewController.h"
#import "RWBusinessBrief.h"
#import "PTNArtifacts/PTNLogger.h"
#import "RWConstants.h"
#import "RWStorage.h"
#import "RWResultsControllerProtocols.h"
#import "RWLocation.h"
#import "PTNArtifacts/PTNAdditions.h"
#import <OHAttributedLabel/NSAttributedString+Attributes.h>

#define RW_STARTUP_ANIMATION_DURATION 0.3

static CGRect sboxLogoViewNormalRect = {{0,0},{0,0}};
static CGRect sboxLogoViewKbdRect = {{0,0},{0,0}};
static CGRect lboxViewNormalRect = {{0,0},{0,0}};
static CGRect lboxViewKbdRect = {{0,0},{0,0}};

@interface RWSearchViewController ()

@property (nonatomic) BOOL isJustLoaded;
@property (nonatomic, strong) CATextLayer *textLayer;

-(void)startLocationUpdates;
-(void)stopLocationUpdates;
-(void)loadSavedLocation;

-(void)performStartUpAnimation;
-(void)keyboardAppearanceChanged:(NSNotification*)notification;
-(void)setLocationButtonTitleWithString:(NSString*)str;
@end

@implementation RWSearchViewController

@synthesize isJustLoaded;
@synthesize searchKeyword;
@synthesize locatingIndicator;
@synthesize sboxLogoView;
@synthesize sboxView;
@synthesize locationView;
@synthesize locationManager;
@synthesize searchBox;
@synthesize locationButton;
@synthesize selectedLocation;
@synthesize textLayer;

//********************************************************************
#pragma mark - received actions
// <#your IBAction's handlers goes here#>

//********************************************************************
#pragma mark - initialization and memory management
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}
//********************************************************************
#pragma mark - view lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
	
    // Do any additional setup after loading the view.    
    [self startLocationUpdates];   
    self.isJustLoaded = YES;
}

- (void)viewDidUnload
{
    [self stopLocationUpdates];
    [self setSearchBox:nil];
    [self setLocationButton:nil];
    [self setSboxLogoView:nil];
    [self setSboxView:nil];
    [self setLocationView:nil];
    [self setLocatingIndicator:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];

    [self subscribeForNotificationsAndSelectors:     
     UIKeyboardWillShowNotification,
     @selector(keyboardAppearanceChanged:),
     UIKeyboardWillHideNotification,
     @selector(keyboardAppearanceChanged:),
     nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self unsubscribeFromNotifications];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (self.isJustLoaded)
        [self performStartUpAnimation];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"ChoseLocation"])
    {
        UINavigationController *nvc = segue.destinationViewController;
        RWLocationViewController *locationVC =  (RWLocationViewController*)nvc.topViewController;
        locationVC.delegate = self;        
    }
    
    if ([[segue identifier] isEqualToString:@"LoadResults"])
    {
        if (!self.selectedLocation)
            [self loadSavedLocation];
        
        RWResultsViewController *nextVC = segue.destinationViewController;
        nextVC.delegate = self;
        nextVC.searchKeyword = self.searchKeyword;
        nextVC.searchLocation = self.selectedLocation;
    }
}

//********************************************************************
#pragma mark - delegation: RWResultsViewController
-(void)resultsViewController:(RWResultsViewController *)vc didChangedSearchKeyword:(NSString *)newKeyword
{
    self.searchKeyword = newKeyword;
    self.searchBox.text = self.searchKeyword;
}
//********************************************************************
#pragma mark - delegation: RWChoseLocationDelegate
-(void)locationViewController:(RWLocationViewController *)locationViewController didChoseLocation:(RWLocation*)place
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if (place)
    {
        [[RWStorage sharedStorageController] setLocation:place];
        [self loadSavedLocation];
    }
}

//********************************************************************
#pragma mark - delegation: CLLocationManagerDelegate
-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    LOG_INFO(@"New location: %@",newLocation);
    [self stopLocationUpdates];
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];

    [geocoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error){
        if (error)
        {
            LOG_WARN(@"Got error while geocoding: %@", error);
            [self loadSavedLocation];
        }
        else {
            CLPlacemark *place = [placemarks objectAtIndex:0];
            LOG_INFO(@"Current location %@", place);
            [[RWStorage sharedStorageController] setLocation:[RWLocation locationWithPlacemark:place]];
            [self loadSavedLocation];
        }
    }];
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    LOG_WARN(@"Got error while locating: %@", error);
    [self stopLocationUpdates];
    [self loadSavedLocation];
}
//********************************************************************
#pragma mark - delegation: UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [searchBox resignFirstResponder];
    
    self.searchKeyword = textField.text;
    [self performSegueWithIdentifier:@"LoadResults" sender:self];              
    return NO;
}

//********************************************************************
#pragma mark - protocol: 
// 

//********************************************************************
#pragma mark - notifications
-(void)keyboardAppearanceChanged:(NSNotification*)notification
{
    if ([notification.name isEqualToString:UIKeyboardWillShowNotification])
    {
        id durationObj = [notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
        id optionsObj = [notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
        double animationDuration = 0.25;
        UIViewAnimationOptions options = UIViewAnimationOptionCurveEaseInOut;
        
        if (durationObj && [durationObj respondsToSelector:@selector(doubleValue)])
            animationDuration = [durationObj doubleValue];
        if (optionsObj && [optionsObj respondsToSelector:@selector(intValue)])
            options = [optionsObj intValue];

        
        if (CGRectIsEmpty(lboxViewKbdRect))
        {
            CGRect kbdFrame;
            [(NSValue*)[notification.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&kbdFrame];
            
            CGRect sboxNewFrame = self.sboxLogoView.frame;
            CGRect lboxNewFrame = self.locationView.frame;
            
            lboxNewFrame.origin.y -= (kbdFrame.size.height-10);
            // search box should be at least 0px higher than location button
            sboxNewFrame.origin.y = (lboxNewFrame.origin.y-(sboxNewFrame.origin.y+sboxNewFrame.size.height) < 0)?lboxNewFrame.origin.y-sboxNewFrame.size.height:sboxNewFrame.origin.y;

            lboxViewKbdRect = lboxNewFrame;
            sboxLogoViewKbdRect = sboxNewFrame;
        }
        
        [UIView animateWithDuration: animationDuration
                              delay:0.0 
                            options:options
                         animations:^(){
                                  self.sboxLogoView.frame = sboxLogoViewKbdRect;
                                  self.locationView.frame = lboxViewKbdRect;
                              }
                         completion:nil];
    }
    
    if ([notification.name isEqualToString:UIKeyboardWillHideNotification])
    {
        [UIView animateWithDuration:[[notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue] 
                              delay:0.0 
                            options:[[notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] intValue] 
                         animations:^(){
                             self.sboxLogoView.frame = sboxLogoViewNormalRect;
                             self.locationView.frame = lboxViewNormalRect;
                         }
                         completion:nil];
    }
}

//********************************************************************
#pragma mark - overriden
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if ([self.searchBox isFirstResponder])
        [self.searchBox resignFirstResponder];
}

//********************************************************************
#pragma mark - class methods
// 

//********************************************************************
#pragma mark - properties
// 

//********************************************************************
#pragma mark - public methods
// 

//********************************************************************
#pragma mark - private methods
-(void)loadSavedLocation
{
    self.locatingIndicator.hidden = YES;
    self.selectedLocation = [[RWStorage sharedStorageController] getSavedLocation];
    
    [self setLocationButtonTitleWithString:[[[RWStorage sharedStorageController] getSavedLocation] getShortName]];
}

-(void)startLocationUpdates
{
    [self.locationButton setTitle:@"Search in..." forState:UIControlStateNormal];
    self.locatingIndicator.hidden = NO;
    
    if (!self.locationManager)
        self.locationManager = [[CLLocationManager alloc] init];
    
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = RW_LOCATION_ACCURACY;
    self.locationManager.distanceFilter = RW_LOCATION_DISTANCE_FILTER;
    [self.locationManager startUpdatingLocation];
}

-(void)stopLocationUpdates
{
    [self.locationManager stopUpdatingLocation];
    self.locationManager = nil;
}

-(void)performStartUpAnimation
{
    [UIView animateWithDuration:RW_STARTUP_ANIMATION_DURATION
                     animations:^(){
                         CGRect frame = self.sboxLogoView.frame;
                         frame.origin.y -= 70;
                         self.sboxLogoView.frame = frame;
                     }
                     completion:^(BOOL finished){
                         sboxLogoViewNormalRect = self.sboxLogoView.frame;
                         lboxViewNormalRect = self.locationView.frame;
                         self.isJustLoaded = NO;                         
                         [UIView animateWithDuration:RW_STARTUP_ANIMATION_DURATION
                                          animations:^(){
                                              self.sboxView.alpha = 1;
                                          }];
                     }];
}

-(void)setLocationButtonTitleWithString:(NSString*)str
{
    [self.locationButton setTitle:@"" forState:UIControlStateNormal];
    
    NSMutableAttributedString *title = [NSMutableAttributedString attributedStringWithString:[NSString stringWithFormat:@"Search in %@",str]];
    [title setFont:self.locationButton.titleLabel.font];
    [title setFontName:[NSString stringWithFormat:@"%@-Bold", self.locationButton.titleLabel.font.fontName] 
                  size:self.locationButton.titleLabel.font.pointSize
                 range:NSMakeRange(10, title.length-10)];
    [title setTextColor:self.locationButton.titleLabel.textColor];

    if (!self.textLayer)
    {
        self.textLayer = [[CATextLayer alloc] init];
        self.textLayer.backgroundColor = [UIColor clearColor].CGColor;
        self.textLayer.wrapped = YES;
        self.textLayer.truncationMode = kCATruncationEnd;
    
        CALayer *layer = self.locationButton.layer;
        self.textLayer.frame = CGRectMake(self.locationButton.titleLabel.frame.origin.x, 
                                  self.locationButton.titleLabel.frame.origin.y,
                                  self.locationButton.bounds.size.width, 
                                  self.locationButton.titleLabel.frame.size.height);
        self.textLayer.contentsScale = [[UIScreen mainScreen] scale]; 
        self.textLayer.alignmentMode = kCAAlignmentLeft;
        [layer addSublayer:self.textLayer];
    }
    
    self.textLayer.string = title;
}
     
@end
