//
//  UIColor+RWThemeColors.h
//  reviewit
//
//  Created by Peter Gusev on 8/29/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

/**
 This is a category which extends UIColor with a bunch of standard review.it colors.
 */

#import <UIKit/UIKit.h>

@interface UIColor (RWThemeColors)

+(UIColor*)rwLightBlueThemeColor;
+(UIColor*)rwBlueThemeColor;
+(UIColor*)rwBlueLinkThemeColor;
+(UIColor*)rwDarkBlueThemeColor;
+(UIColor*)rwGrayThemeColor;
+(UIColor*)rwWhiteThemeColor;

@end
