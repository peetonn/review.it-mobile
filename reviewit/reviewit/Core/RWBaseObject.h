//
//  RWBaseObject.h
//  reviewit
//
//  Created by Peter Gusev on 8/5/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <RestKit/RestKit.h>
#import "RWAPI.h"
#import "RWStorage.h"

@class RWBaseObject;
typedef void(^RWObjectLoadingCallback)(RWBaseObject *obj, NSError *err);
typedef void(^RWObjectArrayLoadingCallback)(NSArray *objArray, NSError *err);
typedef void(^RWRawDataLoadingCallback)(NSData *data, NSError *err);
typedef void(^RWImageLoadingCallback)(RWBaseObject* obj, UIImage *image, NSError *err);

/**
 * RwBaseObject represents base for objects retrieved from remote review.it server. Contains common methods and properties. 
 */
@interface RWBaseObject : NSObject

/**
 * @name Properties
 */
/**
 * ID of the object
 */
@property (nonatomic) NSUInteger ID;

/**
 * @name Class methods
 */
/**
 * Returns RestKit's object mapping necessary for deserializing object from json reply
 */
+(RKObjectMapping*)objectMapping;
/**
 * Initiates asynchronous request for objects with provided mapping on provided resource path
 */
+(void)loadObjectsWithMapping:(RKObjectMapping*)mapping onResourcePath:(NSString*)resourcePath withQueryParameters:(NSDictionary*)queryParams callback:(RWObjectArrayLoadingCallback)block;
/**
 * Initiates asynchronous request for loading some data
 */
+(void)loadDataOnResourcePath:(NSString*)resourcePath withQueryParameters:(NSDictionary*)queryParams callback:(RWRawDataLoadingCallback)block;

@end
