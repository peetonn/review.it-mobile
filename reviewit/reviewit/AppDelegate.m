//
//  AppDelegate.m
//  reviewit
//
//  Created by Peter Gusev on 7/2/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "AppDelegate.h"
#import "RWConstants.h"
#import "PTNArtifacts/PTNLogger.h"
#import "RWStorage.h"
#import "RWDesignController.h"

NSString *const FBSessionStateChangedNotification = @"com.example.Login:FBSessionStateChangedNotification";

@interface AppDelegate ()
@end

@implementation AppDelegate

@synthesize window = _window;

//********************************************************************
#pragma mark - received actions
// <#your IBAction's handlers goes here#>

//********************************************************************
#pragma mark - initialization and memory management
// <#your initialization&memory management code goes here#>


//********************************************************************
#pragma mark - delegation: UIAppDelegate
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self createNewSession];
    [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];
    
    if (![[RWStorage sharedStorageController] wasAppEndedNormally])    
    {
        LOG_WARN(@"Application was not ended normally. Crash log:\n%@",[PTNLogger getLogFile:RW_LOG_FILENAME]);
    }
    else 
        [[RWStorage sharedStorageController] setAppEndedNormally:NO];    
    
    [PTNLogger logToFile:RW_LOG_FILENAME];
    
    LOG_INFO(@"review.it started");
    
    [RWDesignController setupDesign];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBSession.activeSession handleDidBecomeActive];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    [FBSession.activeSession close];
    [[RWStorage sharedStorageController] setAppEndedNormally:YES];
}

-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [FBSession.activeSession handleOpenURL:url];
}

//********************************************************************
#pragma mark - protocol: <#protocol name#>

//********************************************************************
#pragma mark - notifications
// <#your notification's handlers' code goes here#>

//********************************************************************
#pragma mark - overriden
// <#your overide code goes here#>

//********************************************************************
#pragma mark - class methods
// <#your class methods code goes here#>

//********************************************************************
#pragma mark - properties
// <#your customized accessors' code goes here#>

//********************************************************************
#pragma mark - public methods
-(void)openFacebookSessionWithCompletion:(void(^)(FBSession *session, NSError *err))completionBlock
{
    if (FBSession.activeSession.state != FBSessionStateOpen)
    {
        [FBSession.activeSession openWithBehavior:FBSessionLoginBehaviorForcingWebView 
                     completionHandler:^(FBSession *session, FBSessionState state, NSError *error) {
             [self sessionStateChanged:session state:state error:error];
             completionBlock(session,error);
         }];
//        [FBSession openActiveSessionWithReadPermissions:nil
//                                           allowLoginUI:YES
//                                      completionHandler:^(FBSession *session,FBSessionState state,NSError *error) {
//                                          [self sessionStateChanged:session state:state error:error];
//                                          completionBlock(session,error);
//                                      }];
        
    }
    else
        completionBlock(FBSession.activeSession,nil);
}
- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI {
    return [FBSession openActiveSessionWithReadPermissions:nil
                                              allowLoginUI:allowLoginUI
                                         completionHandler:^(FBSession *session,FBSessionState state,NSError *error) {
                                             [self sessionStateChanged:session
                                                                 state:state
                                                                 error:error];
                                         }];
}

//********************************************************************
#pragma mark - private methods
-(void)openSession
{
    [FBSession openActiveSessionWithReadPermissions:nil
                                       allowLoginUI:YES
                                  completionHandler:
     ^(FBSession *session, FBSessionState state, NSError *error) {
         [self sessionStateChanged:session state:state error:error];
     }];
}
- (FBSession *)createNewSession
{
    if (FBSession.activeSession.state == FBSessionStateCreatedTokenLoaded)
        [self openSession];
    return FBSession.activeSession;
}

- (void)sessionStateChanged:(FBSession *)session 
                      state:(FBSessionState) state
                      error:(NSError *)error
{
    switch (state) {
        case FBSessionStateOpen:
            if (!error) {
                // We have a valid session
                LOG_INFO(@"Facebook session found");
            }
            break;
        case FBSessionStateClosed:
        case FBSessionStateClosedLoginFailed:
            [session closeAndClearTokenInformation];
            
            [self createNewSession];
            break;
        default:
            break;
    }
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:FBSessionStateChangedNotification
     object:session];
    
    if (error) {
        UIAlertView *alertView = [[UIAlertView alloc] 
                                  initWithTitle:@"Error"
                                  message:error.localizedDescription
                                  delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil];
        [alertView show];
    }    
}

@end
