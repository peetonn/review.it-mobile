//
//  RWReviewsContainerViewController.m
//  reviewit
//
//  Created by Peter Gusev on 11/25/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "RWReviewsContainerViewController.h"
#import "RWDesignController.h"

@interface RWReviewsContainerViewController ()

@end

@implementation RWReviewsContainerViewController

//********************************************************************
#pragma mark - received actions
-(IBAction)logoutTapped:(id)sender
{
    [self.reviewViewController logout];
}

//********************************************************************
#pragma mark - initialization and memory management
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//********************************************************************
#pragma mark - view lifecycle
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [RWDesignController setNavigationBar:self.navigationController.navigationBar styleShadowed:YES];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationItem.title = self.business.name;
    
    [self informationViewController];
    [self reviewViewController];
    
    self.reviewViewController.business = self.business;
    self.reviewViewController.delegate = self;
    
    [self.informationViewController setLoadingState];
    [self initializeCurrentViewControllerWithViewController:self.informationViewController];
    
    [self.reviewViewController startLoadingReviews];
}

//********************************************************************
#pragma mark - delegation: RWReviewsViewController
-(void)reviewsViewControllerDidStartLoadingComments:(RWReviewsViewController *)aVC
{
    self.navigationItem.rightBarButtonItem.enabled = NO;
    
    [self.informationViewController setLoadingState];
    [self switchViewController:self.currentActiveViewController
              toViewController:self.informationViewController
                 withAnimation:UIViewAnimationOptionTransitionNone
                      duration:0.f
               completionBlock:nil];
}
-(void)reviewsViewController:(RWReviewsViewController *)aVC didErroredLoadingComments:(NSError *)error
{
    self.navigationItem.rightBarButtonItem.enabled = self.reviewViewController.isLoggedIn;
    
    [self.informationViewController setErrorState];
    [self switchViewController:self.currentActiveViewController
              toViewController:self.informationViewController
                 withAnimation:UIViewAnimationOptionTransitionNone
                      duration:0.f
               completionBlock:nil];
}
-(void)reviewsViewControllerDidFinishLoadingComments:(RWReviewsViewController *)aVC
{
    self.navigationItem.rightBarButtonItem.enabled = self.reviewViewController.isLoggedIn;
    
    [self switchViewController:self.currentActiveViewController
              toViewController:self.reviewViewController
                 withAnimation:UIViewAnimationOptionTransitionNone
                      duration:0.f
               completionBlock:nil];
}
//********************************************************************
#pragma mark - delegation: RWResultsInformationViewControllerDelegate
-(void)resultsInformationViewControllerDidTappedTryAgainLink:(RWResultsInformationViewController *)vc
{
    [self.reviewViewController reloadPage];
}

//********************************************************************
#pragma mark - notifications
// <#your notification's handlers' code goes here#>

//********************************************************************
#pragma mark - overriden
// <#your overide code goes here#>

//********************************************************************
#pragma mark - class methods
// <#your class methods code goes here#>

//********************************************************************
#pragma mark - properties
-(RWResultsInformationViewController*)informationViewController
{
    if (!_informationViewController)
    {
        _informationViewController = (RWResultsInformationViewController*)[self loadViewControllerFromNibNamed:@"RWResultsInformationView" ofClass:[RWResultsInformationViewController class]];
        _informationViewController.delegate = self;
    }
    return _informationViewController;
}
-(RWReviewsViewController*)reviewViewController
{
    if (!_reviewsViewController)
    {
        _reviewsViewController = (RWReviewsViewController*)[self loadViewControllerFromStoryboard:self.storyboard named:@"ReviewsViewController"];
    }
    return _reviewsViewController;
}

//********************************************************************
#pragma mark - public methods
// _

//********************************************************************
#pragma mark - private methods
-(void)navigateBack
{
    if ([self.currentActiveViewController isEqual:self.reviewViewController])
    {
        if ([self.reviewViewController canNavigateBack])
        {
            [self.reviewViewController navigateBack];
            return;
        }
    }

    [self.navigationController popViewControllerAnimated:YES];
}

@end
