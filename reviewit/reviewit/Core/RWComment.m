//
//  RWComment.m
//  reviewit
//
//  Created by Peter Gusev on 10/21/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "RWComment.h"
#import "RWUser.h"

@interface RWComment ()

@property (nonatomic, retain) NSString *fbCommentID;
@property (nonatomic, retain) NSString *fbUserID;
@property (nonatomic, retain) NSString *fbUserName;
@property (nonatomic) NSTimeInterval createdTimestamp;
@property (nonatomic, retain) NSString *status;

@end

@implementation RWComment

//********************************************************************
#pragma mark - received actions
// <#your IBAction's handlers goes here#>

//********************************************************************
#pragma mark - initialization and memory management
-(id)init
{
    if ((self = [super init]))
    {
        self.wasViewed = NO;
        self.viewHeight = 0;
    }
    return self;
}

//********************************************************************
#pragma mark - delegation: <#sender#>
// <#your delegated methods goes here#>

//********************************************************************
#pragma mark - protocol: <#protocol name#>
// <#your conformed protocol code goes here#>

//********************************************************************
#pragma mark - notifications
// <#your notification's handlers' code goes here#>

//********************************************************************
#pragma mark - overriden
// <#your overide code goes here#>

//********************************************************************
#pragma mark - class methods
+(RKObjectMapping*)objectMapping
{
    RKObjectMapping *commentMapping = [RKObjectMapping mappingForClass:[RWComment class]];
    
    [commentMapping mapKeyPath:RW_API_JSON_REV_ID toAttribute:@"ID"];
    [commentMapping mapKeyPath:RW_API_JSON_REV_MESSAGE toAttribute:@"message"];
    [commentMapping mapKeyPath:RW_API_JSON_REV_STATUS toAttribute:@"status"];
    [commentMapping mapKeyPath:RW_API_JSON_REV_CREATED toAttribute:@"createdTimestamp"];
    [commentMapping mapKeyPath:RW_API_JSON_REV_FBUSERID toAttribute:@"fbUserID"];
    [commentMapping mapKeyPath:RW_API_JSON_REV_FBUSERNAME toAttribute:@"fbUserName"];
    [commentMapping mapKeyPath:RW_API_JSON_REV_FBCOMMENTID toAttribute:@"fbCommentID"];
    
    return commentMapping;
}

+(void)reviewsForBusiness:(RWBusinessBrief *)business limit:(NSUInteger)limit page:(NSUInteger)page callback:(RWObjectArrayLoadingCallback)block
{
    NSDictionary *queryParams = [NSDictionary dictionaryWithKeysAndObjects:
                                 RW_API_QUERY_REV_BUSINESSID,[NSNumber numberWithInt: business.ID],
                                 RW_API_QUERY_REV_LIMIT, [NSNumber numberWithInt: limit],
                                 RW_API_QUERY_REV_PAGE, [NSNumber numberWithInt: page],
                                 nil];
    NSString *resourcePath = [[RWStorage sharedStorageController] getReviewsResourcePathString];
    
    [RWBaseObject loadObjectsWithMapping:[RWComment objectMapping]
                          onResourcePath:resourcePath
                     withQueryParameters:queryParams
                                callback:^(NSArray *reviews, NSError *err){
                                    if (!err)
                                    {
                                        __block int nReviews = [reviews count];
                                        
                                        if (nReviews)
                                        {
                                            NSEnumerator *en = [reviews objectEnumerator];
                                            RWComment *review = nil;
                                            while ((review = [en nextObject]))
                                            {
                                                // load user data such as user profile image
                                                [review loadUserData:^(RWBaseObject *review, NSError *error){
                                                    nReviews--;
                                                    // upon completion of loading data for last review - deliver data
                                                    if (!nReviews)
                                                        block(reviews,err);
                                                }];
                                            }
                                        }
                                        else
                                        {
                                            block(reviews,err);
                                        }
                                    }
                                    else
                                    {
                                        block(reviews,err);
                                    }
                                }];
    
}

//********************************************************************
#pragma mark - properties
-(NSString*)createdString
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateStyle:NSDateFormatterMediumStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    return [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:self.createdTimestamp]];
}

//********************************************************************
#pragma mark - public methods
-(void)loadUserData:(RWObjectLoadingCallback)onLoadFinished
{
    [RWUser userWithID:self.fbUserID callback:^(RWBaseObject *user, NSError *err){
        self.user = (RWUser*)user;
        self.user.name = self.fbUserName;
        
        onLoadFinished(self,err);
    }];
}

//********************************************************************
#pragma mark - private methods
// 

@end
