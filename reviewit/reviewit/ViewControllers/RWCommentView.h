//
//  RWCommentView.h
//  reviewit
//
//  Created by Peter Gusev on 10/21/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

/**
 This is a special-purpose view that's used in a RWCommentCell.
 */

#import <UIKit/UIKit.h>
#import "PTNArtifacts/PTNControls.h"

#define RW_REVIEW_MIN_HEIGHT 100

@interface RWCommentView : UIView<PTNExpandableLabelDelegate>

@property (weak, nonatomic) IBOutlet id delegate;
@property (weak, nonatomic) IBOutlet PTNExpandableLabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userLogoImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIView *infoAreaView;

@end

@protocol RWCommentViewDelegate <NSObject>

-(void)commentView:(RWCommentView*)commentView messageExpandedWithAnimationDuration:(NSTimeInterval)duration forNewSize:(CGSize)newSize;
-(void)commentViewDidFinishedMessageExpansion:(RWCommentView*)commentView;

@end
