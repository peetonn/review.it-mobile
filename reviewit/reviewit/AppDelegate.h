//
//  AppDelegate.h
//  reviewit
//
//  Created by Peter Gusev on 7/2/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

/**
 This class is obviously the starting point of the app. It sets up the Facebook session,
 and calls the RWDesignController to set up the user interface appearance. The flow is then
 handled by the main storyboard.
 
 The app acts as a client to the review.it database. The communication is handled by a bunch
 of HTTP calls to the server, which return results in JSON format. The parsing of these
 results is handled with the RestKit library.
 
 RestKit: http://restkit.org/
 JSON: http://en.wikipedia.org/wiki/JSON
 */

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "PTNArtifacts/PTNLogger.h"
#import <RestKit/RestKit.h>

extern NSString *const FBSessionStateChangedNotification;

@interface AppDelegate : UIResponder <UIApplicationDelegate, CLLocationManagerDelegate>

@property (strong, nonatomic) UIWindow *window;

-(void)openFacebookSessionWithCompletion:(void(^)(FBSession *session, NSError *err))completionBlock;
-(BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI;

@end
