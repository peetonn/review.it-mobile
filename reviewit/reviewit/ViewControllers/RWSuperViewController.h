//
//  RWSuperViewController.h
//  reviewit
//
//  Created by Peter Gusev on 8/28/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

/**
 Base class for RWResultsViewController, RWReviewsViewController and
 RWSearchViewController. Doesn't do much, but locks the screen into
 portrait.
 */

#import <UIKit/UIKit.h>

@interface RWSuperViewController : UIViewController

@end
