//
//  RWResultsViewController.h
//  reviewit
//
//  Created by Peter Gusev on 8/6/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

/**
 * In general - all search results screens should be presented using following idea:
 * - there is one container view controller, which coordinates several search resutls content view controlers
 * - container view controller is a data source (see RWSearchResultsDataSource) for content view controllers
 * - content view controllers are responsible for making search requests and retrieving new results from server
 * - content view controllers present results in specific way (like lists, maps, etc.) which are not relying on containter view controller
 This class handles the following duties:
 1. Showing a status screen for searching (loading, error), by using the RWResultsInformationViewController class
 1. Flipping between showing the search results in a list or a map, respectively
    RWListResultsViewController or RWMapResultsViewController
 1. Displaying the search bar and handling its input
 
 */

#import <UIKit/UIKit.h>
#import "RWListResultsViewController.h"
#import "RWMapResultsViewController.h"
#import "PTNArtifacts/PTNControls.h"
#import "RWResultsControllerProtocols.h"
#import "RWResultsInformationViewController.h"
#import "RWContainerViewController.h"

@interface RWResultsViewController : RWContainerViewController
<UISearchBarDelegate, RWSearchResultsDataController,
PTNCoverViewDelegate, RWResultsInformationViewControllerDelegate>
{
    RWListResultsViewController *_listResultsViewController;
    RWMapResultsViewController *_mapResultsViewController;
    NSString *_searchKeyword;
}

/**
 * @name Protperties
 */
@property (weak, nonatomic) IBOutlet id delegate;
@property (weak, nonatomic, readonly) RWListResultsViewController *listResultsViewController;
@property (weak, nonatomic, readonly) RWMapResultsViewController *mapResultsViewController;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *flipViewControllerButton;
@property (strong, nonatomic) IBOutlet PTNCoverView *coverView;

/**
 * @name Received actions
 */
- (IBAction)flipControllers:(id)sender;

@end

@protocol RWResultsViewControllerDelegate <NSObject>

-(void)resultsViewController:(RWResultsViewController*)vc didChangedSearchKeyword:(NSString*)newKeyword;

@end
