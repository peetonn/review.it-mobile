//
//  RWSearchResultsControllerProtocol.h
//  reviewit
//
//  Created by Peter Gusev on 8/1/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "RWLocation.h"

typedef enum _RWSearchResult {
    RWSearchResultOK = 0,
    RWSearchResultError = 1,
    RWSearchResultEmpty = 2,
    RWSearchResultLoading = 3
} RWSearchResult;

@protocol RWSearchResultsContentProtocol;
@protocol RWSearchResultsDataController;

/**
 * Protocol which is used by specific search results view controllers (search results content view controllers) in order to retrieve and update search results data.
 */
@protocol RWSearchResultsDataController <NSObject>

@required
/**
 * @name Properties
 */
/**
 * Search keyword used for searching
 */
@property (nonatomic, strong) NSString *searchKeyword;
/**
 * Current search results
 */
@property (nonatomic, strong) NSArray *searchResults;
/**
 * Current search location
 */
@property (nonatomic, strong) RWLocation *searchLocation;

/**
 * @name Methods
 */
/**
 * Used by content view controller when new results are available
 */
-(void)searchResultsContentViewController:(id<RWSearchResultsContentProtocol>)contentViewController providedNewResults:(NSArray*)searchResults withSearchResult:(RWSearchResult)newResult;

/**
 * Used by content view controller when more results are available
 */
-(void)searchResultsContentViewController:(id<RWSearchResultsContentProtocol>)contentViewController providedMoreResults:(NSArray*)searchResults withSearchResult:(RWSearchResult)newResult;
/**
 * Notifies container controller that content controller is going to start search
 */
-(void)searchResultsContentViewControllerWillStartSearch:(id<RWSearchResultsContentProtocol>)contentViewController;
/**
 * Notifies container controller that content controller did finish search
 */
-(void)searchResultsContentViewControllerDidFinishSearch:(id<RWSearchResultsContentProtocol>)contentViewController;
@end

/**
 * This protocol defines properties and methods for content view controllers of search results
 * There are could be multiple view controllers displaying search results. This protocol prescribes what
 * exactly these view controllers should implement in order to be used by search resutls container view controller. 
 */
@protocol RWSearchResultsContentProtocol <NSObject>
@required
/**
 * @name Properties
 */
/**
 * Indicates current content's search state (RWSearchResult)
 */
@property (nonatomic, readonly) RWSearchResult searchResultState;
/**
 * Container view controller, which conforms to RWResultsContainerDataSource
 */
@property (nonatomic, readonly) id<RWSearchResultsDataController> searchResultsDataController;
/**
 * @name Instance methods
 */
/**
 * Initiates search for businesses. All parameters should be get from container view controller
 */
-(void)initiateSearch;
/**
 * Notify content view controller that current search results state changed
 */
-(void)didCurrentSearchResultsStateChanged:(RWSearchResult)newResult;

@end
