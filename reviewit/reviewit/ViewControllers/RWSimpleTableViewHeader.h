//
//  RWSimpleTableViewHeader.h
//  reviewit
//
//  Created by Peter Gusev on 8/28/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RWSimpleTableViewHeader : UIView

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
