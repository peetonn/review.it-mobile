//
//  RWProfileCell.m
//  reviewit
//
//  Created by Peter Gusev on 9/11/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "RWProfileCell.h"
#import "PTNArtifacts/NSString+PTNAdditions.h"
#import "UIColor+RWThemeColors.h"
#import "PTNArtifacts/PTNAdditions.h"

@implementation RWProfileCell
@synthesize profileView;

//********************************************************************
#pragma mark - received actions
// <#your IBAction's handlers goes here#>

//********************************************************************
#pragma mark - initialization and memory management
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

//********************************************************************
#pragma mark - delegation: <#sender#>
// <#your delegated methods goes here#>

//********************************************************************
#pragma mark - protocol: <#protocol name#>
// <#your conformed protocol code goes here#>

//********************************************************************
#pragma mark - notifications
// <#your notification's handlers' code goes here#>

//********************************************************************
#pragma mark - overriden
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

//********************************************************************
#pragma mark - class methods
// <#your class methods code goes here#>

//********************************************************************
#pragma mark - properties
-(void)setBusiness:(RWBusinessBrief *)business
{
    _business = business;
    self.profileView.businessTitleLabel.text = _business.name;
    self.profileView.businessCategoryLabel.text = _business.category.name;
    NSString *strippedURLString = [[[NSURL URLWithString:_business.website] host] stringByReplacingOccurrencesOfString:@"www." withString:@""];
    
    [self.profileView.businessWebsiteButton setTitle:strippedURLString
                                            forState:UIControlStateNormal];
    self.profileView.businessCategoryImageView.image = _business.category.icon;
    self.profileView.businessDescriptionLabel.text = _business.description;

    [self setButton:self.profileView.businessPhoneButton withTitle:_business.details.phoneNumber];
    [self setButton:self.profileView.businessAddressButton withTitle:_business.details.address];
    
    self.profileView.reviewsNumLabel.text = [NSString stringWithFormat:@"%d",_business.reviewsNum];
    self.profileView.reviewsTextLabel.text = (_business.reviewsNum == 1)?@"review":@"reviews";
    self.profileView.businessImageView.image = _business.category.placeholderImage;
    
//    [_business loadImageInBlock:^(RWBaseObject *business, UIImage *logo, NSError *err){
//        if (((RWBusinessBrief*)business).hasLogo)
//        {
//            CGRect croppingRect = CGRectMake(0, _business.logo.size.height/2-self.profileView.businessImageView.frame.size.height/2, self.profileView.businessImageView.frame.size.width, self.profileView.businessImageView.frame.size.height);
//            logo = [UIImage imageWithImage:logo aspectScaledAndCropped:croppingRect];
//        }
//        
//        self.profileView.businessImageView.image = logo;
//    }];
}
-(RWBusinessBrief*)business
{
    return _business;
}

//********************************************************************
#pragma mark - public methods
// 

//********************************************************************
#pragma mark - private methods
-(void)setButton:(UIButton*)button withTitle:(NSString*)title
{
    if (!title || [title isEqualToString:@""])
    {
        button.enabled = NO;
        button.titleLabel.font = [UIFont italicSystemFontOfSize:14.f];
    }
    else
    {
        button.titleLabel.font = [UIFont systemFontOfSize:14.f];
        [button setTitle:title forState:UIControlStateNormal];
    }
}


@end
