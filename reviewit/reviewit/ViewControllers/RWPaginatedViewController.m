//
//  RWPaginatedViewController.m
//  reviewit
//
//  Created by Peter Gusev on 12/1/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "RWPaginatedViewController.h"

@interface RWPaginatedViewController ()
@end

@implementation RWPaginatedViewController

//********************************************************************
#pragma mark - received actions
// <#your IBAction's handlers goes here#>

//********************************************************************
#pragma mark - initialization and memory management
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        [self initialize];
    }
    return self;
}
-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self initialize];
    }
    return self;
}
-(void)initialize
{
    self.lastRequestEmpty = NO;
    self.isLoadingNextPage = NO;
    self.currentLoadedPage = 0;
}

//********************************************************************
#pragma mark - delegation: <#sender#>
// <#your delegated methods goes here#>

//********************************************************************
#pragma mark - protocol: <#protocol name#>
// <#your conformed protocol code goes here#>

//********************************************************************
#pragma mark - notifications
// <#your notification's handlers' code goes here#>

//********************************************************************
#pragma mark - overriden
// <#your overide code goes here#>

//********************************************************************
#pragma mark - class methods
// <#your class methods code goes here#>

//********************************************************************
#pragma mark - properties
// <#your customized accessors' code goes here#>

//********************************************************************
#pragma mark - public methods
-(BOOL)hasMorePages
{
    return !self.lastRequestEmpty;
}
-(BOOL)shouldAddFunctionalCell
{
    return [self hasMorePages] || self.isLoadingNextPage;
}
//********************************************************************
#pragma mark - private methods
// <#your private methods' code goes here#>

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSUInteger functionCell = [self shouldAddFunctionalCell] ? 1 : 0;
    return functionCell;
}

#pragma mark - Table view delegate

@end
