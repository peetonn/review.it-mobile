//
//  RWResultsInformationViewController.m
//  reviewit
//
//  Created by Peter Gusev on 8/16/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "RWResultsInformationViewController.h"
#import "UIColor+RWThemeColors.h"

#define RW_FANCY_LABEL_EMPH_SIZE 68

#define RW_INFO_VIEW_TRYAGAIN_URL @"http://tryagain.com"

static NSAttributedString *ErrorText = nil;
static NSAttributedString *NoResultsText = nil;

@interface RWResultsInformationViewController ()
@property (nonatomic, weak) UIView *currentView;

@property (nonatomic, readonly) NSAttributedString *errorText;
@property (nonatomic, readonly) NSAttributedString *noResultsText;

-(void)setCurrentVisibleView:(UIView*)view;
-(void)setInformationLabelText:(NSAttributedString*)str withLinkText:(NSString *)linkText andURL:(NSURL*)url;
-(NSAttributedString*)getFancyStringNamed:(NSString*)strName andEmphasizedNamed:(NSString*)emphName;
@end

@implementation RWResultsInformationViewController
@synthesize loadingView;
@synthesize informationView;
@synthesize loadingIndicator;
@synthesize informationLabel;
@synthesize currentView;

//********************************************************************
#pragma mark - received actions
// <#your IBAction's handlers goes here#>

//********************************************************************
#pragma mark - initialization and memory management
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

//********************************************************************
#pragma mark - view lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [self setLoadingView:nil];
    [self setInformationView:nil];
    [self setInformationLabel:nil];
    [self setLoadingIndicator:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

//********************************************************************
#pragma mark - delegation: OHAttributedLabelDelegate
-(BOOL)attributedLabel:(OHAttributedLabel *)attributedLabel shouldFollowLink:(NSTextCheckingResult *)linkInfo
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(resultsInformationViewControllerDidTappedTryAgainLink:)])
        [self.delegate resultsInformationViewControllerDidTappedTryAgainLink:self];
    
    return NO;
}

//********************************************************************
#pragma mark - protocol: <#protocol name#>
// <#your conformed protocol code goes here#>

//********************************************************************
#pragma mark - notifications
// <#your notification's handlers' code goes here#>

//********************************************************************
#pragma mark - overriden
// <#your overide code goes here#>

//********************************************************************
#pragma mark - class methods
// <#your class methods code goes here#>

//********************************************************************
#pragma mark - properties
-(RWInformationViewState)state
{
    return _state;
}
-(NSAttributedString*)errorText
{
    if (!ErrorText)
        ErrorText = [self getFancyStringNamed:@"Search results error text" andEmphasizedNamed:@"Search results error emphasized text"];
    
    return ErrorText;
}
-(NSAttributedString*)noResultsText
{
    if (!NoResultsText)
        NoResultsText = [self getFancyStringNamed:@"No results text" andEmphasizedNamed:@"No results emphasized text"];
    
    return NoResultsText;
}

//********************************************************************
#pragma mark - public methods
-(void)setErrorState
{
    [self setInformationLabelText:self.errorText
                     withLinkText:NSLocalizedStringFromTable(@"Search results error link text", @"RWLocalized", @"")
                           andURL:[NSURL URLWithString:RW_INFO_VIEW_TRYAGAIN_URL]];
    [self setCurrentVisibleView:self.informationView];
    _state = RWErrorState;
}    

-(void)setEmptyResultsState
{
    [self setInformationLabelText:self.noResultsText withLinkText:nil andURL:nil];
    [self setCurrentVisibleView:self.informationView];
    _state = RWEmptyResultsState;
}

-(void)setLoadingState
{
    [self setCurrentVisibleView:self.loadingView];
    _state = RWLoadingState;
}

-(void)setStateForSearchResultState:(RWSearchResult)resultsState
{
    switch (resultsState) {
        case RWSearchResultEmpty:
            [self setEmptyResultsState];
            break;
        case RWSearchResultError:
            [self setErrorState];
            break;
        case RWSearchResultLoading:
            [self setLoadingState];
            break;            
        default:
            break;
    }
}
//********************************************************************
#pragma mark - private methods
-(void)setInformationLabelText:(NSAttributedString*)str withLinkText:(NSString *)linkText andURL:(NSURL*)url
{
    informationLabel.delegate = self;
    [informationLabel setLinkColor:[UIColor rwBlueLinkThemeColor]];
    [informationLabel setLinkUnderlineStyle:kCTUnderlineStyleNone];

    NSMutableAttributedString *astr = [NSMutableAttributedString attributedStringWithAttributedString:str];
//    [astr setTextBold:NO range:[str.string rangeOfString:linkText]];
    
    [self.informationLabel setAttributedText:astr];
    
    if (url && linkText)
        [self.informationLabel addCustomLink:url inRange:[str.string rangeOfString:linkText]];
}
-(void)setCurrentVisibleView:(UIView *)view
{
    [self.currentView removeFromSuperview];
    
    view.frame = self.view.bounds;
    [self.view addSubview:view];
    self.currentView = view;
}

-(NSAttributedString*)getFancyStringNamed:(NSString*)strName andEmphasizedNamed:(NSString*)emphName
{
    NSString *text = NSLocalizedStringFromTable(strName, @"RWLocalized", @"");
    NSString *emphasizedText = NSLocalizedStringFromTable(emphName, @"RWLocalized", @"");

    NSMutableAttributedString *astr = [NSMutableAttributedString attributedStringWithString:text];
    [astr setFont:self.informationLabel.font];
    [astr setFontName:[NSString stringWithFormat:@"%@-Bold",self.informationLabel.font.fontName]
                          size:RW_FANCY_LABEL_EMPH_SIZE
                         range:[text rangeOfString:emphasizedText]];
    [astr setTextColor:self.informationLabel.textColor];
    [astr setTextAlignment:kCTTextAlignmentCenter lineBreakMode:kCTLineBreakByWordWrapping];
    
    return astr;
}

@end
