//
//  RWBusinessDetails.m
//  reviewit
//
//  Created by Peter Gusev on 8/8/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "RWBusinessDetails.h"
#import <CoreLocation/CoreLocation.h>
#import "PTNArtifacts/PTNLogger.h"

@implementation RWBusinessDetails

@synthesize reviewsNum, phoneNumber, workingHours, latitude, longitude, address;

//********************************************************************
#pragma mark - received actions
// <#your IBAction's handlers goes here#>

//********************************************************************
#pragma mark - initialization and memory management

//********************************************************************
#pragma mark - delegation: <#sender#>
// <#your delegated methods goes here#>

//********************************************************************
#pragma mark - protocol: <#protocol name#>
// <#your conformed protocol code goes here#>

//********************************************************************
#pragma mark - notifications
// <#your notification's handlers' code goes here#>

//********************************************************************
#pragma mark - overriden
// <#your overide code goes here#>

//********************************************************************
#pragma mark - class methods
+(RKObjectMapping*)objectMapping
{
    RKObjectMapping *businessDetailsMapping = [RKObjectMapping mappingForClass:[RWBusinessDetails class]];
    
    [businessDetailsMapping mapKeyPath:@"id" toAttribute:@"ID"];
    [businessDetailsMapping mapKeyPath:@"lat" toAttribute:@"latitude"];
    [businessDetailsMapping mapKeyPath:@"lon" toAttribute:@"longitude"];
    [businessDetailsMapping mapKeyPath:@"address" toAttribute:@"address"];
    [businessDetailsMapping mapKeyPath:@"phone" toAttribute:@"phoneNumber"];
    [businessDetailsMapping mapKeyPath:@"hours" toAttribute:@"workingHours"];
    
    return businessDetailsMapping;
}


//********************************************************************
#pragma mark - properties

//********************************************************************
#pragma mark - public methods
// <#your public methods' code goes here#>

//********************************************************************
#pragma mark - private methods


@end
