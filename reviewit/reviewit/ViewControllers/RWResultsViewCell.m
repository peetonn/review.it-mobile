//
//  RWResultsViewself.m
//  reviewit
//
//  Created by Peter Gusev on 8/6/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "RWResultsViewCell.h"
#import "PTNArtifacts/PTNLogger.h"

@implementation RWResultsViewCell

@synthesize titleLabel, categoryLabel, logo, descriptionLabel, categoryIcon, reviewsNumLabel;

//********************************************************************
#pragma mark - received actions
// <#your IBAction's handlers goes here#>

//********************************************************************
#pragma mark - initialization and memory management
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)dealloc
{
    self.categoryIcon = nil;
    self.reviewsNumLabel = nil;
    self.descriptionLabel = nil;
    self.titleLabel = nil;
    self.categoryLabel = nil;
    self.logo = nil;
}

//********************************************************************
#pragma mark - view lifecycle
// <#your view lifecycle code goes here#>

//********************************************************************
#pragma mark - delegation: <#sender#>
// <#your delegated methods goes here#>

//********************************************************************
#pragma mark - protocol: <#protocol name#>
// <#your conformed protocol code goes here#>

//********************************************************************
#pragma mark - notifications
// <#your notification's handlers' code goes here#>

//********************************************************************
#pragma mark - overriden
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
//********************************************************************
#pragma mark - class methods
// <#your class methods code goes here#>

//********************************************************************
#pragma mark - properties
-(void)setBusiness:(RWBusinessBrief *)business
{
    _business = business;
    self.titleLabel.text = _business.name;
    self.categoryLabel.text = _business.category.name;
    self.descriptionLabel.text = _business.description;
    self.reviewsNumLabel.text = [NSString stringWithFormat:@"%d",_business.reviewsNum];
    
    self.categoryIcon.image = _business.category.icon;
    self.logo.image = _business.category.placeholderImage;
    
    __block RWResultsViewCell *cell = self;
    [_business loadImageInBlock:^(RWBaseObject *business, UIImage *image, NSError *err){
        if (!err && cell.business == business)
        {
            cell.logo.image = image;
        }
    }];
}
-(RWBusinessBrief*)business
{
    return _business;
}

//********************************************************************
#pragma mark - public methods
// <#your public methods' code goes here#>

//********************************************************************
#pragma mark - private methods
// <#your private methods' code goes here#>

@end
