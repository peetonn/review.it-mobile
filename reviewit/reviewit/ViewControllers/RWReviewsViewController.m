//
//  RWReviewsViewController.m
//  reviewit
//
//  Created by Peter Gusev on 7/19/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "RWReviewsViewController.h"
#import "RWConstants.h"
#import "RWStorage.h"
#import "PTNArtifacts/PTNLogger.h"
#import "RWDesignController.h"
#import "RWStorage.h"

#define RW_FB_LOGINSUCCESS_URL_PATH @"/plugins/login_success.php"
#define RW_FB_ITUNES_HOST @"itunes.apple.com"

// number of frames for loading FB comments plugin
#define RW_FB_COMMENTS_FRAME_COUNTER 8

@interface RWReviewsViewController ()

@property (nonatomic) NSUInteger loadedFrameCounter;

-(void)loadReviews;

@end

@implementation RWReviewsViewController
@synthesize reviewsView;

//********************************************************************
#pragma mark - received actions
// <#your IBAction's handlers goes here#>

//********************************************************************
#pragma mark - initialization and memory management
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

    }
    return self;
}

//********************************************************************
#pragma mark - view lifecycle
- (void)viewDidUnload
{
    reviewsView.delegate = nil;
    [self setReviewsView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

//********************************************************************
#pragma mark - delegation: UIWebView
-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    LOG_ERROR(@"failed to load FB comments plugin due to error: %@",error);
    
    // workaround for redirecting from "login success" page
    NSRange range = [self.reviewsView.request.URL.absoluteString rangeOfString:RW_FB_LOGINSUCCESS_URL_PATH];
    if (range.location == NSNotFound)
    {
        if (self.delegate && [self.delegate respondsToSelector:@selector(reviewsViewController:didErroredLoadingComments:)])
            [self.delegate reviewsViewController:self didErroredLoadingComments:error];
    }
}
-(void)webViewDidStartLoad:(UIWebView *)webView
{
}
-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    self.loadedFrameCounter++;
    if (self.loadedFrameCounter == RW_FB_COMMENTS_FRAME_COUNTER && [self isReviewsPageActive])
        if (self.delegate && [self.delegate respondsToSelector:@selector(reviewsViewControllerDidFinishLoadingComments:)])
            [self.delegate reviewsViewControllerDidFinishLoadingComments:self];
}
-(BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{

    switch (navigationType) {
        case UIWebViewNavigationTypeLinkClicked:
            LOG_TRACE(@"link clicked");
            break;
        case UIWebViewNavigationTypeFormSubmitted:
            LOG_TRACE(@"form submitted");
            break;
        case UIWebViewNavigationTypeBackForward:
            LOG_TRACE(@"back forward");
            break;
        case UIWebViewNavigationTypeReload:
            LOG_TRACE(@"reload");
            break;
        case UIWebViewNavigationTypeFormResubmitted:
            LOG_TRACE(@"form resubmitted");
            break;
        case UIWebViewNavigationTypeOther:
            LOG_TRACE(@"other");
            break;
        default:
            break;
    }
    
    if ([self isReviewsPageActive])
    {
        if (navigationType == UIWebViewNavigationTypeReload)
            [self performSelectorOnMainThread:@selector(loadReviews) withObject:nil waitUntilDone:NO];
        else if (!(navigationType == UIWebViewNavigationTypeOther))
            {
                LOG_INFO(@"push user to browser with URL: %@",request.URL);
                [[UIApplication sharedApplication] openURL:request.URL];
            }
        
        return (navigationType == UIWebViewNavigationTypeOther);
    }
    else
    {
        if ([request.URL.relativePath isEqual:RW_FB_LOGINSUCCESS_URL_PATH])
        {
            [self.reviewsView stopLoading];
            // at this point loading interruption will cause error which is caught in
            // -(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error method
            [self performSelectorOnMainThread:@selector(loadReviews) withObject:nil waitUntilDone:NO];
            return NO;
        }

        return YES;
    }
}

//********************************************************************
#pragma mark - protocol: <#protocol name#>
// <#your conformed protocol code goes here#>

//********************************************************************
#pragma mark - notifications
// <#your notification's handlers' code goes here#>

//********************************************************************
#pragma mark - overriden
// <#your overide code goes here#>

//********************************************************************
#pragma mark - class methods
// <#your class methods code goes here#>

//********************************************************************
#pragma mark - properties
-(BOOL)isLoggedIn
{
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    
    __block BOOL fbCookie = NO;
    [storage.cookies enumerateObjectsUsingBlock:^(NSHTTPCookie *cookie, NSUInteger idx, BOOL *stop){
        *stop = fbCookie = [cookie.domain isEqualToString:@".facebook.com"] && [cookie.name isEqualToString:@"c_user"];;
    }];

    return fbCookie;
}
//********************************************************************
#pragma mark - public methods
-(void)logout
{
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    
    for (NSHTTPCookie *cookie in [storage cookies])
    {
        if ([cookie.domain isEqualToString:@".facebook.com"])
            [storage deleteCookie:cookie];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self loadReviews];
}
-(void)startLoadingReviews
{
    [self loadReviews];
}
-(void)navigateBack
{
    if (self.reviewsView.canGoBack)
        [self.reviewsView goBack];
}
-(BOOL)canNavigateBack
{
    return (![self isReviewsPageActive] && reviewsView.canGoBack);
}
-(void)reloadPage
{
//    if ([self isReviewsPageActive])
        [self loadReviews];
//    else
//        [reviewsView reload];
}
//********************************************************************
#pragma mark - private methods
-(BOOL)isReviewsPageActive
{
    return [reviewsView.request.URL isEqual:[[RWStorage sharedStorageController] getFBCommentsURLForBusinessID: self.business.ID]];
}-(void)loadURL:(NSURL*)URL
{
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    [reviewsView loadRequest:request];
}
-(void)loadReviews
{
    self.loadedFrameCounter = 0;
    [self loadURL:[[RWStorage sharedStorageController] getFBCommentsURLForBusinessID: self.business.ID]];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(reviewsViewControllerDidStartLoadingComments:)])
        [self.delegate reviewsViewControllerDidStartLoadingComments:self];
}


@end
