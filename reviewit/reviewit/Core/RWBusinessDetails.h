//
//  RWBusinessDetails.h
//  reviewit
//
//  Created by Peter Gusev on 8/8/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RWBaseObject.h"

/**
 * Class represents advanced details of the business like working hours, cooridnates, address, etc.
 */
@interface RWBusinessDetails : RWBaseObject
/**
 * @name Properties
 */
/**
 * Coordinates of physical location of the business
 */
@property (nonatomic) double latitude;
@property (nonatomic) double longitude;

/**
 * Address  
 */
@property (nonatomic, retain) NSString *address;
/**
 * Number of reviews
 */
@property (nonatomic) NSUInteger reviewsNum;
/**
 * Phone number
 */
@property (nonatomic,retain) NSString *phoneNumber;
/**
 * Working hours
 */
@property (nonatomic, retain) NSString *workingHours;

@end
