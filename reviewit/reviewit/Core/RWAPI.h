//
//  RWAPI.h
//  reviewit
//
//  Created by Peter Gusev on 10/22/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#ifndef reviewit_RWAPI_h
#define reviewit_RWAPI_h

// JSON fields
// business json
#define RW_API_JSON_BIS_ID @"id"
#define RW_API_JSON_BIS_NAME @"name"
#define RW_API_JSON_BIS_DESCRIPTION @"description"
#define RW_API_JSON_BIS_REVIEWSNUM @"review_count"
#define RW_API_JSON_BIS_LOGO @"logo"
#define RW_API_JSON_BIS_WEBSITE @"website"
#define RW_API_JSON_BIS_CATEGORY @"industry"
#define RW_API_JSON_BIS_DETAILS @"branch"

// review json
#define RW_API_JSON_REV_ID @"id"
#define RW_API_JSON_REV_MESSAGE @"message"
#define RW_API_JSON_REV_STATUS @"status"
#define RW_API_JSON_REV_CREATED @"created"
#define RW_API_JSON_REV_FBUSERID @"fb_user_id"
#define RW_API_JSON_REV_FBUSERNAME @"fb_user_name"
#define RW_API_JSON_REV_FBCOMMENTID @"fb_comment_id"
#define RW_API_JSON_REV_BUSINESSID @"business_id"

// QUERIES parameters' names
// business info
#define RW_API_QUERY_BIS_SEARCH @"q"
#define RW_API_QUERY_BIS_CLIENT @"client"
#define RW_API_QUERY_BIS_PAGE @"page"
#define RW_API_QUERY_BIS_LIMIT @"limit"

// reviews
#define RW_API_QUERY_REV_BUSINESSID @"business_id"
#define RW_API_QUERY_REV_PAGE @"page"
#define RW_API_QUERY_REV_LIMIT @"limit"

#endif
