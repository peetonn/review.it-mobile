//
//  reviewitTests.m
//  reviewitTests
//
//  Created by Peter Gusev on 7/2/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "reviewitTests.h"
#import "RWBusinessBrief.h"
#import "RWStorage.h"

@implementation reviewitTests

- (void)setUp
{
    [super setUp];
    
    err = NO;
    success = NO;
    storage = [[RWStorage alloc] initWithStorageFile:RW_SETTINGS_FILE];
}

- (void)tearDown
{
    // Tear-down code here.
    [super tearDown];
}

-(void)testSuccessLoading
{
    [self tryLoading];
    STAssertTrue(success, @"loading objects was unsuccessful");
}   

-(void)testFailedLoading
{
    NSString *newWrongAPIVersion = @"wrongAPIVersion";
    NSString *oldAPIVersion = [[RWStorage sharedStorageController] getCurrentAPIVersion];
    [[RWStorage sharedStorageController] setCurrentAPIVersion:newWrongAPIVersion];
    
    [self tryLoading];
    STAssertTrue(err, @"loading objects was successful");
    
    [[RWStorage sharedStorageController] setCurrentAPIVersion:oldAPIVersion];
}

- (void)tryLoading
{
    success = NO;

    [RWBusinessBrief businessBriefsForKeyword:@"villa" andLocation:nil completionBlock:^(NSArray *briefs, NSError *error){
        if (error)
        {
            NSLog(@"Failed loading due to error: %@", error);
            err = YES;
        }
        else {
            if (briefs)
                [briefs enumerateObjectsUsingBlock:^(RWBusinessBrief *brief, NSUInteger idx, BOOL *stop){
                    NSLog(@"%@ %d",brief.name, brief.ID);
                }];
        }
            
    }];
    
    [self nonBlockWaitForFlag1:&success orFlag2:&err];
}

-(void)nonBlockWaitForFlag1:(BOOL*)flag1 orFlag2:(BOOL*)flag2
{
    while (!*flag1 && !*flag2) {
        [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.2]];
    }
}

-(void)nonBlockWaitForSeconds:(NSUInteger)seconds
{
    [[NSRunLoop currentRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:seconds]];    
}

@end
