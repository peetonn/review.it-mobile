//
//  RWStorage.h
//  reviewit
//
//  Created by Peter Gusev on 7/26/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "PTNArtifacts/PTNStorage.h"
#import "RWConstants.h"
#import "RWLocation.h"

/**
 * Class represents storage of application settings saved in RW_SETTINGS_FILE 
 */
@interface RWStorage : PTNStorage

/**
 * @name Class methods
 */
/**
 * Returns shared instance of RWStorage controller
 */
+(RWStorage*)sharedStorageController;

/**
 * @name Instance methods
 */
/**
 * Returns base URL for current API version
 */
-(NSString*)getBaseURLStringForCurrentAPI;

/**
 * Returns current API version
 */
-(NSString*)getCurrentAPIVersion;

/**
 * Sets new API version
 */
-(void)setCurrentAPIVersion:(NSString*)apiVersion;

/**
 * Returns quick search resource path
 */
-(NSString*)getQuickSearchResourcePathString;

/**
 * Returns reviews resource path
 */
-(NSString*)getReviewsResourcePathString;

/**
 * Updates saved location
 */
-(void)setLocation:(RWLocation*)location;

/**
 * Returns previously saved location 
 */
-(RWLocation*)getSavedLocation;

/**
 * Returns array of recent locations
 */
-(NSArray*)getRecentLocations;

/**
 * Adds new recent location
 */
-(void)addRecentLocation:(RWLocation*)location;

/**
 * Returns current client type
 */
-(NSString*)getCurrentClient;

/**
 * Returns loading review limit for profile screen
 */
-(NSUInteger)getLoadingReviewsLimit;

/**
 * Returns profile loading URL format
 */
-(NSString*)getProfileURLStringForUserID:(NSString*)userID;

/**
 * Returns URL for web page of facebook comments plugin for provided business ID
 */
-(NSURL*)getFBCommentsURLForBusinessID:(NSUInteger)businessID;

/**
 * Returns URL for business logo
 */
-(NSString*)getBusinessLogoURLStringWithName:(NSString*)logoName;

/**
 * Returns attributed string for promo bubble on profile screen
 */
-(NSAttributedString*)getBubblePromoString;

/**
 * Returns bubble's hyperlinked text
 */
-(NSString*)getBubbleLinkText;

/**
 * Returns bubble's link
 */
-(NSURL*)getBubbleURL;

/**
 * Returns numebr of search results to be loaded at once
 */
-(NSUInteger)resultsLimit;

/**
 * Returns location search default radius
 */
-(float)locationSearchRadius;

@end
