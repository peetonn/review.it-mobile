//
//  RWSimpleCell.m
//  reviewit
//
//  Created by Peter Gusev on 8/28/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "RWSimpleCell.h"

@implementation RWSimpleCell

@synthesize textLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)dealloc
{
    self.textLabel = nil;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
