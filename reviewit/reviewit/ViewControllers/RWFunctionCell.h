//
//  RWFunctionCell.h
//  reviewit
//
//  Created by Peter Gusev on 10/23/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

/**
 
 */

#import <UIKit/UIKit.h>

@interface RWFunctionCell : UITableViewCell
{
    BOOL _isLoadingState;
}

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;

-(void)configureViewForPath:(NSIndexPath*)indexPath;

@end
