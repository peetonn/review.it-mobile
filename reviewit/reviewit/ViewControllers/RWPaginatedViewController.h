//
//  RWPaginatedViewController.h
//  reviewit
//
//  Created by Peter Gusev on 12/1/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RWPaginatedViewController : UITableViewController

/**
 * @name Properties
 */
/**
 * Indicates, whether last request returned any results
 */
@property (nonatomic) BOOL lastRequestEmpty;
/**
 * Indicates, whether loading request for next page is in progress
 */
@property (nonatomic) BOOL isLoadingNextPage;
/**
 * Shows the number of loaded pages
 */
@property (nonatomic) NSUInteger currentLoadedPage;

/**
 * @name Instance methods
 */
/**
 * Initializes structures
 */
-(void)initialize;
/**
 * Indicates, whether there are more pages to load.
 * Should be overrided, as its returning value is based just
 * on results of last request (lastRequestEmpty)
 */
-(BOOL)hasMorePages;
/**
 * Indicates, whether table view should have functional cell -
 * a cell with activity indicator which shows progress of loading 
 * new pages
 */
-(BOOL)shouldAddFunctionalCell;

@end
