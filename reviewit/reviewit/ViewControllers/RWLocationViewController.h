//
//  RWLocationViewController.h
//  reviewit
//
//  Created by Peter Gusev on 7/13/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "RWLocation.h"
#import "RWSimpleTableViewHeader.h"

@protocol RWChoseLocationDelegate;

@interface RWLocationViewController : UITableViewController<UISearchDisplayDelegate>{
    NSArray *recentPlaces;
    NSMutableArray *searchResults;
    CLGeocoder *geocoder;
}

@property (weak, nonatomic) id<RWChoseLocationDelegate> delegate;
@property RWLocation *selectedLocation;

@property (strong, nonatomic) IBOutlet RWSimpleTableViewHeader *tableViewHeader;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (strong, nonatomic) IBOutlet UISearchDisplayController *searchDisplayController;

- (IBAction)cancel:(id)sender;
- (IBAction)done:(id)sender;

@end

@protocol RWChoseLocationDelegate <NSObject>

-(void)locationViewController:(RWLocationViewController*)locationViewController didChoseLocation:(RWLocation*)location;

@end
