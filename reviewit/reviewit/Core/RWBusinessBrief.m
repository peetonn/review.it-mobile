//
//  RWBusinessBrief.m
//  reviewit
//
//  Created by Peter Gusev on 7/25/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "RWBusinessBrief.h"
#import "PTNArtifacts/PTNLogger.h"
#import "NSError+RWErrors.h"

@interface RWBusinessBrief ()
@property (nonatomic, retain) NSString *logoStringName;
@end

@implementation RWBusinessBrief

@synthesize name, category, reviewsNum, details, description, logoStringName, website;

//********************************************************************
#pragma mark - received actions
// <#your IBAction's handlers goes here#>

//********************************************************************
#pragma mark - initialization and memory management
// <#your initialization&memory management code goes here#>

//********************************************************************
#pragma mark - delegation: MKAnnotation protocol
-(CLLocationCoordinate2D)coordinate
{
    return CLLocationCoordinate2DMake(self.details.latitude, self.details.longitude);
}
-(NSString*)title
{
    return [NSString stringWithFormat:@"%@ (%d reviews)", self.name, self.reviewsNum];
}
-(NSString*)subtitle
{
    return self.details.address;
}

//********************************************************************
#pragma mark - protocol: <#protocol name#>
// <#your conformed protocol code goes here#>

//********************************************************************
#pragma mark - notifications
// <#your notification's handlers' code goes here#>

//********************************************************************
#pragma mark - overriden

//********************************************************************
#pragma mark - class methods
+(RKObjectMapping*)objectMapping
{
    RKObjectMapping *businessBriefMapping = [RKObjectMapping mappingForClass:[RWBusinessBrief class]];

    [businessBriefMapping mapKeyPath:RW_API_JSON_BIS_ID toAttribute:@"ID"];
    [businessBriefMapping mapKeyPath:RW_API_JSON_BIS_NAME toAttribute:@"name"];
    [businessBriefMapping mapKeyPath:RW_API_JSON_BIS_DESCRIPTION toAttribute:@"description"];
    [businessBriefMapping mapKeyPath:RW_API_JSON_BIS_REVIEWSNUM toAttribute:@"reviewsNum"];
    [businessBriefMapping mapKeyPath:RW_API_JSON_BIS_LOGO toAttribute:@"logoStringName"];
    [businessBriefMapping mapKeyPath:RW_API_JSON_BIS_WEBSITE toAttribute:@"website"];
    [businessBriefMapping mapKeyPath:RW_API_JSON_BIS_CATEGORY toRelationship:@"category" withMapping:[RWIndustry objectMapping]];
    [businessBriefMapping mapKeyPath:RW_API_JSON_BIS_DETAILS toRelationship:@"details" withMapping:[RWBusinessDetails objectMapping]];
    
    return businessBriefMapping;
}

+(void)businessBriefsForKeyword:(NSString*)keyword
                       location:(RWLocation*)location
                          limit:(NSUInteger)limit
                         pageNo:(NSUInteger)page
                completionBlock:(RWObjectArrayLoadingCallback)block
{
    if (!keyword)
    {
        block([NSArray array], nil);
        return;
    }
    
    NSMutableDictionary *queryParams = [NSMutableDictionary dictionaryWithKeysAndObjects:
                                 RW_API_QUERY_BIS_CLIENT,[[RWStorage sharedStorageController] getCurrentClient],
                                 RW_API_QUERY_BIS_SEARCH,keyword,
                                 nil];
    
    if (limit > 0)
        [queryParams setObject:[NSNumber numberWithInt:limit] forKey:RW_API_QUERY_BIS_LIMIT];
    if (page > 0)
        [queryParams setObject:[NSNumber numberWithInt:page] forKey:RW_API_QUERY_BIS_PAGE];    
    
    NSString *resourcePath = [[RWStorage sharedStorageController] getQuickSearchResourcePathString];
    
    [RWBaseObject loadObjectsWithMapping:[RWBusinessBrief objectMapping]
                          onResourcePath:resourcePath
                     withQueryParameters:queryParams
                                callback:^(NSArray *briefsArray, NSError *err){
                                    if (err)
                                        LOG_ERROR(@"error while retrieveing businesses: %@", err);
                                    block(briefsArray,err);
                                }];
}

+(void)businessBriefsForKeyword:(NSString*)keyword
                      nearLocation:(CLLocation*)location
                            withRadius:(float)metersRadius
                   completionBlock:(RWObjectArrayLoadingCallback)block
{
    __block int pageNo = 1;
    int limit = 500; // limit is intentionally set to big number - to load as much as possible
    
    [RWBusinessBrief businessBriefsForKeyword:keyword
                                     location:nil
                                        limit:limit
                                       pageNo:pageNo
                              completionBlock:^(NSArray *objArray, NSError *err){
                                  if (objArray)
                                  {
                                      __block int nBusinessesToCheck = [objArray count];
                                      __block NSMutableArray *filteredBusinesses = [NSMutableArray array];
                                      for (RWBusinessBrief *business in objArray)
                                      {
                                          if (business.canGeocode)
                                          {
                                              // start geocoding business
                                              [business geocodeLocationCallback:^(RWBaseObject *obj, NSError *err){
                                                  RWBusinessBrief *geocodedBusiness = (RWBusinessBrief*)obj;
                                                  nBusinessesToCheck--;
                                                  // if business lies near the location - add to filtered objects
                                                  if (!err && [geocodedBusiness isLyingNearLocation:location withinDistance:metersRadius])
                                                  {
                                                      [filteredBusinesses addObject:geocodedBusiness];
                                                  }
                                                  // if all businesses were checked - call callback
                                                  if (!nBusinessesToCheck)
                                                      block(filteredBusinesses, nil);
                                              }];
                                          }
                                          else
                                          {
                                              nBusinessesToCheck--;
                                              if (!business.isGeocodingNeeded && [business isLyingNearLocation:location withinDistance:metersRadius])
                                                  [filteredBusinesses addObject:business];
                                          }
                                      } // for
                                      if (!nBusinessesToCheck)
                                          block(filteredBusinesses,nil);
                                  }
                                  else
                                      block(objArray, err);
                              }];
}
//********************************************************************
#pragma mark - properties
-(BOOL)canGeocode
{
    return (self.isGeocodingNeeded && self.details.address);
}
-(BOOL)isGeocodingNeeded
{
    return (!self.details.latitude && !self.details.longitude);
}
-(BOOL)hasLogo
{
    return self.logoStringName && ![self.logoStringName isEqualToString:@""];
}
-(UIImage*)logo
{
    return _logo;
}
-(CLLocation*)location
{
    return [[CLLocation alloc] initWithLatitude:self.details.latitude longitude:self.details.longitude];
}
//********************************************************************
#pragma mark - public methods
-(BOOL)isLyingNearLocation:(CLLocation *)location withinDistance:(float)radius
{
    return ([self.location distanceFromLocation:location] <= radius);
}
-(void)loadImageInBlock:(RWImageLoadingCallback)block
{
    if (_logo)
    {
        block(self,_logo,nil);
    }
    else
    {
        if (!self.hasLogo)
        {
            block(self,[RWIndustry placeholderImageForIndustry:self.category],nil);
        }
        else
        {
            NSString *resourcePath = [[RWStorage sharedStorageController] getBusinessLogoURLStringWithName:self.logoStringName];
            [RWBaseObject loadDataOnResourcePath:resourcePath
                            withQueryParameters:nil
                                        callback:^(NSData *data, NSError *error){
                                            if (!error)
                                                _logo = [UIImage imageWithData:data];
                                            else
                                                LOG_ERROR(@"Error while loading business logo: %@", error);
                                            block(self,_logo,error);
                                        }];
        }
    }
}

//********************************************************************
#pragma mark - private methods
-(void)geocodeLocationCallback:(RWObjectLoadingCallback)block
{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    
    [geocoder geocodeAddressString:self.details.address completionHandler:^(NSArray *placemarks, NSError *error){
        if (!error)
        {
            CLPlacemark *placemark = [placemarks objectAtIndex:0];
            self.details.latitude = placemark.location.coordinate.latitude;
            self.details.longitude = placemark.location.coordinate.longitude;
            
            if (block)
                block(self,nil);
        }
        else
        {
            LOG_ERROR(@"error while geocoding business address: %@",error);
            
            if (block)
                block(self,error);
        }
    }];
}

@end
