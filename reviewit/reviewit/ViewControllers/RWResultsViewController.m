//
//  RWResultsViewController.m
//  reviewit
//
//  Created by Peter Gusev on 8/6/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "RWResultsViewController.h"
#import "RWStorage.h"
#import "RWBusinessBrief.h"
#import "PTNArtifacts/PTNLogger.h"
#import "RWDesignController.h"

#define RW_PLISTKEY_LAST_SCREEN @"Last search results screen"
#define RW_PLISTVALUE_LIST_SCREEN @"List"
#define RW_PLISTVALUE_MAP_SCREEN @"Map"
#define RW_FLIPANIMATION_DURATION 0.5

@interface RWResultsViewController ()

@property (nonatomic, strong) UIViewController<RWSearchResultsContentProtocol> *currentResultsViewController;

-(void)loadMapResultsViewController;
-(void)loadListResultsViewController;

-(void)loadLastUsedResultsView;

-(void)setCoverViewHidden:(BOOL)hidden animated:(BOOL)animated;
-(void)setCancelButtonHidden:(BOOL)hidden animated:(BOOL)animated;

-(void)setButtonImageIsMap:(BOOL)isButtonImageMap;
@end

@implementation RWResultsViewController
@synthesize searchBar;
@synthesize currentResultsViewController;
@synthesize flipViewControllerButton;
@synthesize coverView;
@synthesize searchResults = _searchResults, searchLocation, searchKeyword = _searchKeyword;

//********************************************************************
#pragma mark - received actions
-(IBAction)flipControllers:(id)sender {
    if ([self.searchBar isFirstResponder])
        [self searchBarCancelButtonClicked:self.searchBar];
    
    if ([self.currentResultsViewController isKindOfClass:[RWListResultsViewController class]])
    {
        [self setButtonImageIsMap:NO];
        [self loadMapResultsViewController];
    }
    else
    {
        [self setButtonImageIsMap:YES];
        [self loadListResultsViewController];
    }
}

//********************************************************************
#pragma mark - initialization and memory management
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)dealloc
{
    _listResultsViewController = nil;
    _mapResultsViewController = nil;
}

//********************************************************************
#pragma mark - view lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.searchResults = [NSArray array];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.navigationItem.backBarButtonItem = [RWDesignController defaultBackButtonWithTarget:self.navigationController
                                                                                     action:@selector(popViewControllerAnimated:)];
    [RWDesignController setSearchBar:self.searchBar styleShadowed:YES];    
    self.searchBar.text = _searchKeyword;
    
    // force view controller to load child view controllers
    [self listResultsViewController];
    [self mapResultsViewController];
    
    [self loadLastUsedResultsView];

    [self initializeCurrentViewControllerWithViewController:self.currentResultsViewController];
    [self.currentResultsViewController initiateSearch];
}
- (void)viewDidUnload
{
    [self.currentActiveViewController.view removeFromSuperview];
    
    [self setFlipViewControllerButton:nil];
    [self setContentView:nil];
    [self setSearchBar:nil];
    [self setCoverView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    
    [self adjustCurrentViewToSuperview];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [RWDesignController setNavigationBar:self.navigationController.navigationBar styleShadowed:NO];
}
//********************************************************************
#pragma mark - delegation: UISearchBarDelegate
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self searchBarTextDidEndEditing:self.searchBar];
    
    // don't initiate search with the same keyword
//    if ([self.searchKeyword isEqualToString:self.searchBar.text] && !([self.currentActiveViewController isEqual:self.informationViewController] && self.informationViewController.state == RWErrorState))
//        return;
    
    // don't initiate search with empty keyword
    if ([self.searchBar.text isEqualToString:@""])
    {
        self.searchBar.text = self.searchKeyword;
        return;
    }
    
    self.searchKeyword = self.searchBar.text;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(resultsViewController:didChangedSearchKeyword:)])
        [self.delegate resultsViewController:self didChangedSearchKeyword:self.searchKeyword];
    
    [self.currentResultsViewController initiateSearch];
}
-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    self.searchBar.text = self.searchKeyword;
    [self.searchBar resignFirstResponder];
}
-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    [self.searchBar resignFirstResponder];
    [self setCoverViewHidden:YES animated:YES];
    [self setCancelButtonHidden:YES animated:YES];
}
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self setCoverViewHidden:NO animated:YES];
    [self setCancelButtonHidden:NO animated:YES];
}

//********************************************************************
#pragma mark - delegation: PTNCoverViewDelegate
-(void)coverViewWasTapped:(PTNCoverView *)coverView
{
    [self searchBarCancelButtonClicked:self.searchBar];
}

//********************************************************************
#pragma mark - delegation: RWResultsInformationViewControllerDelegate
-(void)resultsInformationViewControllerDidTappedTryAgainLink:(RWResultsInformationViewController *)vc
{
    [self.currentResultsViewController initiateSearch];
}

//********************************************************************
#pragma mark - protocol conformance: RWSearchResultsDataSource
-(void)searchResultsContentViewController:(id)contentViewController
                      providedMoreResults:(NSArray *)results
                         withSearchResult:(RWSearchResult)newResult
{
    NSMutableArray *newResults = [NSMutableArray array];
    for (RWBusinessBrief *business in results)
    {
        if (![self.searchResults containsObject:business])
            [newResults addObject:business];
    }
    
    self.searchResults = [self.searchResults arrayByAddingObjectsFromArray:newResults];
    [self notifyNonActiveContentControllersWithNewSearchResult:newResult];
}
-(void)searchResultsContentViewController:(id<RWSearchResultsContentProtocol>)contentViewController
                       providedNewResults:(NSArray *)searchResults
                         withSearchResult:(RWSearchResult)newResult
{
    self.searchResults = searchResults;
    [self notifyNonActiveContentControllersWithNewSearchResult:newResult];
}
-(void)searchResultsContentViewControllerWillStartSearch:(id<RWSearchResultsContentProtocol>)contentViewController
{
    self.flipViewControllerButton.enabled = NO;
}
-(void)searchResultsContentViewControllerDidFinishSearch:(id<RWSearchResultsContentProtocol>)contentViewController
{
    self.flipViewControllerButton.enabled = YES;
}
-(void)setSearchKeyword:(NSString *)aSearchKeyword
{
    _searchKeyword = aSearchKeyword;
    searchBar.text = _searchKeyword;
}

//********************************************************************
#pragma mark - notifications
// <#your notification's handlers' code goes here#>

//********************************************************************
#pragma mark - overriden
-(void)setCurrentActiveViewController:(UIViewController *)currentActiveViewController
{
    [super setCurrentActiveViewController:currentActiveViewController];
    self.flipViewControllerButton.enabled = [self.currentActiveViewController conformsToProtocol:@protocol(RWSearchResultsContentProtocol)];
}

//********************************************************************
#pragma mark - class methods
// <#your class methods code goes here#>

//********************************************************************
#pragma mark - properties
-(RWListResultsViewController*)listResultsViewController
{
    if (!_listResultsViewController)
    {
        _listResultsViewController = (RWListResultsViewController*)[self loadViewControllerFromStoryboard:self.storyboard named:@"ListResultsViewController"];
    }
    return _listResultsViewController;
}
-(RWMapResultsViewController*)mapResultsViewController
{
    if (!_mapResultsViewController)
    {
        _mapResultsViewController = (RWMapResultsViewController*)[self loadViewControllerFromStoryboard:self.storyboard named:@"MapResultsViewController"];
    }
    return _mapResultsViewController;
}
//********************************************************************
#pragma mark - public methods

//********************************************************************
#pragma mark - private methods
-(void)notifyNonActiveContentControllersWithNewSearchResult:(RWSearchResult)newResult
{
    if (self.currentResultsViewController != self.listResultsViewController)
        [self.listResultsViewController didCurrentSearchResultsStateChanged:(RWSearchResult)newResult];
    else
        [self.mapResultsViewController didCurrentSearchResultsStateChanged:(RWSearchResult)newResult];
}
-(void)loadListResultsViewController
{
    [self switchViewController:self.currentActiveViewController
            toViewController:self.listResultsViewController
               withAnimation:UIViewAnimationOptionTransitionFlipFromLeft
                    duration:RW_FLIPANIMATION_DURATION     
             completionBlock:^(){
                 self.currentResultsViewController = self.listResultsViewController;                 
                 [[RWStorage sharedStorageController] saveParam:RW_PLISTVALUE_LIST_SCREEN forKey:RW_PLISTKEY_LAST_SCREEN];                 
             }];
}
-(void)loadMapResultsViewController
{
    [self switchViewController:self.currentActiveViewController
            toViewController:self.mapResultsViewController
               withAnimation:UIViewAnimationOptionTransitionFlipFromRight
                    duration:RW_FLIPANIMATION_DURATION          
             completionBlock:^(){
                 self.currentResultsViewController = self.mapResultsViewController;
                 [[RWStorage sharedStorageController] saveParam:RW_PLISTVALUE_MAP_SCREEN forKey:RW_PLISTKEY_LAST_SCREEN];
             }];

}
-(void)setCoverViewHidden:(BOOL)hidden animated:(BOOL)animated
{
    if (!hidden)
    {
        self.coverView.alpha = 0;
        [self.contentView addSubview:self.coverView];
        self.coverView.frame = self.contentView.bounds;
        [UIView animateWithDuration:0.2
                         animations:^(){
                             self.coverView.alpha = 0.8;
                         }];
    }
    else {
        [UIView animateWithDuration:0.2
                         animations:^(){
                             self.coverView.alpha = 0;
                         }
                         completion:^(BOOL completion){
                             [self.coverView removeFromSuperview];
                         }];
    }
}
-(void)setCancelButtonHidden:(BOOL)hidden animated:(BOOL)animated
{
    [self.searchBar setShowsCancelButton:!hidden animated:animated];
}
-(void)loadLastUsedResultsView
{
    NSString *lastScreen = [[RWStorage sharedStorageController] getParamWithName:RW_PLISTKEY_LAST_SCREEN];
    
    if ([lastScreen isEqualToString:RW_PLISTVALUE_LIST_SCREEN])
    {
        [self setButtonImageIsMap:YES];    
        self.currentResultsViewController = self.listResultsViewController;
    }
    else
    {
        [self setButtonImageIsMap:NO];
        self.currentResultsViewController = self.mapResultsViewController;
    }
}
-(void)setButtonImageIsMap:(BOOL)isButtonImageMap
{
    [self.navigationItem.rightBarButtonItem setBackgroundImage:[[UIImage imageNamed:@"navbarbutton.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)]
                                                      forState:UIControlStateNormal
                                                    barMetrics:UIBarMetricsDefault];

    if (!isButtonImageMap)
        self.navigationItem.rightBarButtonItem.image = [UIImage imageNamed:@"flip_btn_list.png"];            
    else 
        self.navigationItem.rightBarButtonItem.image = [UIImage imageNamed:@"flip_btn_map.png"];            
}
@end
