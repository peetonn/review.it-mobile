//
//  UIColor+RWThemeColors.m
//  reviewit
//
//  Created by Peter Gusev on 8/29/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "UIColor+RWThemeColors.h"

@implementation UIColor (RWThemeColors)

+(UIColor*)rwLightBlueThemeColor
{
    return [UIColor colorWithRed:0./255. green:197./255. blue:255./255. alpha:1.];
}
+(UIColor*)rwBlueThemeColor
{
    return [UIColor colorWithRed:0./255. green:185./255. blue:237./255. alpha:1.];
}
+(UIColor*)rwBlueLinkThemeColor
{
    return [UIColor colorWithRed:0./255. green:210./255. blue:255./255. alpha:1.];
}
+(UIColor*)rwDarkBlueThemeColor
{
    return [UIColor colorWithRed:0./255. green:155./255. blue:200./255. alpha:1.];
}
+(UIColor*)rwGrayThemeColor
{
    return [UIColor colorWithWhite:.35 alpha:1.];
}
+(UIColor*)rwWhiteThemeColor
{
    return [UIColor colorWithRed:1. green:1. blue:1. alpha:1.];
}
@end
