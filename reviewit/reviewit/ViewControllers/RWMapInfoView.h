//
//  RWMapInfoView.h
//  reviewit
//
//  Created by Peter Gusev on 12/3/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RWResultsControllerProtocols.h"

@interface RWMapInfoView : UIView

@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (nonatomic, weak) IBOutlet UILabel *infoLabel;

-(void)setInfoStateForSearchResult:(RWSearchResult)state;

@end
