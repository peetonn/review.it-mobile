//
//  RWProfileCell.h
//  reviewit
//
//  Created by Peter Gusev on 9/11/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

/**
 Handles the layout of the business description (the RWBusinessBrief) and uses the
 RWProfileView to handle the layout part of this.
 */

#import <UIKit/UIKit.h>
#import "RWProfileView.h"
#import "RWBusinessBrief.h"

@interface RWProfileCell : UITableViewCell
<RWProfileViewDelegate>
{
    __weak RWBusinessBrief *_business;
}

@property (nonatomic, weak) IBOutlet RWProfileView *profileView;

@property (nonatomic, weak) RWBusinessBrief *business;

@end
