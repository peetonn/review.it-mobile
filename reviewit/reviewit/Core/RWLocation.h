//
//  RWLocation.h
//  reviewit
//
//  Created by Peter Gusev on 8/14/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

/**
 * Size of recent locations list
 */
const NSUInteger RWRecentLocationsNum;

/**
 * RWLocation represents geographical location. Location is presented in textual form - country, locality (region), sublocality (city). RWLocation also can be initialized from CLPlacemark.
 */
@interface RWLocation : NSObject
{
    @private
    CLPlacemark *_placemark;
}

/**
 * @name Properties
 */
/**
 * Country of location
 */
@property (nonatomic, copy) NSString *country;
/**
 * Region associated with this location. 
 * In some cases it could represent city
 */
@property (nonatomic, copy) NSString *locality;
/**
 * City associated with this location
 */
@property (nonatomic, copy) NSString *subLocality;
/**
 * Full textual name of location, separated by commas
 */
@property (nonatomic, readonly) NSString *readableName;
/**
 * Placemark associated with this location
 */
@property (nonatomic, readonly) CLPlacemark *placemark;
/**
 * Coordinates of this location
 */
@property (nonatomic, readonly) CLLocation *location;
/**
 * RWLocation saved to dictionary
 */
@property (nonatomic, readonly) NSDictionary *dictionary;
/**
 * Indicates, whether geocoding needed for this location. 
 * In case location was created with dictionary, there is no exact
 * locaiton info about this place (placemark property will return nil)
 * for such cases geocodeLocationWithCallback: should be used.
 */
@property (nonatomic, readonly) BOOL isGeocodingNeeded;

/**
 * @name Class methods
 */
/**
 * Initializes new instance from CLPlacemark
 */
+(RWLocation*)locationWithPlacemark:(CLPlacemark*)placemark;
/**
 * Initializes new instance from dictionary
 */
+(RWLocation*)locationFromDictionary:(NSDictionary*)dictionary;
/**
 * @name Instance methods
 */
/**
 * Initialization with dictionary
 * Dictionary can contain values for following keys: 
 *  RW_LOCATION_KEY_COUNTRY
 *  RW_LOCATION_KEY_LOCALITY
 *  RW_LOCATION_KEY_SUBLOCALITY
 */
-(id)initWithDictionary:(NSDictionary*)dictionary;
/**
 * Initialization with associated CLPlacemark
 */
-(id)initWithPlacemark:(CLPlacemark*)placemark;
/**
 * Returns short name, which is based on the most narrow available 
 * geographic name, e.g. city, region, country.
 */
-(NSString*)getShortName;
/**
 * Geocodes placemark for current location
 */
-(void)geocodeLocationWithCallback:(void(^)(RWLocation *location, NSError *err))block;

@end
