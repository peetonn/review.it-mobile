//
//  RWAnnotationView.m
//  reviewit
//
//  Created by Peter Gusev on 8/22/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "RWAnnotationView.h"
#import "RWBusinessBrief.h"

@implementation RWAnnotationView

//********************************************************************
#pragma mark - received actions
// <#your IBAction's handlers goes here#>

//********************************************************************
#pragma mark - initialization and memory management
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

-(id)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self)
    {
        _normalImage = ((RWBusinessBrief*)annotation).category.mapPinImage;
        _highlightedImage = ((RWBusinessBrief*)annotation).category.mapPinImageSelected;
        self.image = _normalImage;
        self.rightCalloutAccessoryView = [[[NSBundle mainBundle] loadNibNamed:@"RWDisclosureButton" owner:self options:nil] objectAtIndex:0];        
        
        self.centerOffset = CGPointMake(6, -16);
        self.calloutOffset = CGPointMake(-5, 0); 
    }
    return self;
}

//********************************************************************
#pragma mark - delegation: <#sender#>
// <#your delegated methods goes here#>

//********************************************************************
#pragma mark - protocol: <#protocol name#>
// <#your conformed protocol code goes here#>

//********************************************************************
#pragma mark - notifications
// <#your notification's handlers' code goes here#>

//********************************************************************
#pragma mark - overriden
-(void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    
    if (selected)
        self.image = _highlightedImage;
    else
        self.image = _normalImage;
}

-(void)setAnnotation:(id<MKAnnotation>)annotation
{
    [super setAnnotation:annotation];
    
    _normalImage = ((RWBusinessBrief*)annotation).category.mapPinImage;
    _highlightedImage = ((RWBusinessBrief*)annotation).category.mapPinImageSelected;
    self.image = _normalImage;    
}

//********************************************************************
#pragma mark - class methods
// <#your class methods code goes here#>

//********************************************************************
#pragma mark - properties
// <#your customized accessors' code goes here#>

//********************************************************************
#pragma mark - public methods
// <#your public methods' code goes here#>

//********************************************************************
#pragma mark - private methods
// <#your private methods' code goes here#>

@end
