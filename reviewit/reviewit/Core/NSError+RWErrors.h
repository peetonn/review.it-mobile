//
//  NSError+RWErrors.h
//  reviewit
//
//  Created by Peter Gusev on 12/3/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSInteger const RWErrorBadArgument;

@interface NSError (RWErrors)

+(NSError*)errorWithCode:(NSInteger)code message:(NSString*)msg;

@end
