//
//  RWUser.h
//  reviewit
//
//  Created by Peter Gusev on 10/21/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "RWBaseObject.h"

@interface RWUser : RWBaseObject

/**
 * @name Properties
 */
/**
 * Name of the user
 */
@property (nonatomic, retain) NSString *name;
/**
 * User's image
 */
@property (nonatomic, retain) UIImage *image;


/**
 * Class methods
 */
/**
 * Retrievs user from cache based on id provided, if there is no such user - loads it and returns it in block parameters 
 */
+(void)userWithID:(NSString*)userID callback:(RWObjectLoadingCallback)block;

@end
