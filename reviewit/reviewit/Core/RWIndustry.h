//
//  RWIndustry.h
//  reviewit
//
//  Created by Peter Gusev on 8/5/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RWBaseObject.h"

/**
 * RWIndustry represents type of industry to which business belongs
 */
@interface RWIndustry : RWBaseObject

/**
 * @name Properties
 */
/**
 * Name of the industry
 */
@property (nonatomic, retain) NSString *name;

/**
 * Status of industry
 */
@property (nonatomic, retain) NSString *status;

/**
 * Category icon
 */
@property (nonatomic, readonly) UIImage *icon;

/**
 * Category map pin icon
 */
@property (nonatomic, readonly) UIImage *mapPinImage;

/**
 * Category selected map pin icon
 */
@property (nonatomic, readonly) UIImage *mapPinImageSelected;

/**
 * Category placeholder image
 */
@property (nonatomic, readonly) UIImage *placeholderImage;

/**
 * @name Class methods
 */
+(UIImage*)placeholderImageForIndustry:(RWIndustry*)industry;

@end
