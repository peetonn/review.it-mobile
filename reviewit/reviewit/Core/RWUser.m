//
//  RWUser.m
//  reviewit
//
//  Created by Peter Gusev on 10/21/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "RWUser.h"
#import "PTNArtifacts/PTNLogger.h"

static NSMutableDictionary *cachedUsers = nil;

@implementation RWUser

+(void)userWithID:(NSString *)userID callback:(RWObjectLoadingCallback)block
{
    if (!cachedUsers)
        cachedUsers = [[NSMutableDictionary alloc] init];
    
    __block RWUser *user = nil;
    if ((user = [cachedUsers objectForKey:userID]))
        block(user,nil);
    else // load image
    {
        user = [[RWUser alloc] init];
        NSString *resourcePath = [[RWStorage sharedStorageController] getProfileURLStringForUserID:userID];
        
        [RWBaseObject loadDataOnResourcePath:resourcePath withQueryParameters:nil callback:^(NSData *data, NSError *error){
            if (!error)
                user.image = [UIImage imageWithData:data];
            else
                LOG_ERROR(@"Error while loading user picture: %@", error);
            [cachedUsers setObject:user forKey:userID];
            
            block(user,error);
        }];
    }
}

@end
