//
//  RWConstants.h
//  reviewit
//
//  Created by Peter Gusev on 7/17/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#ifndef reviewit_RWConstants_h
#define reviewit_RWConstants_h

#define RW_LOG_FILENAME     @"reviewit.log"
#define RW_SETTINGS_FILE    @"RWSettings"

#define RW_LOCATION_ACCURACY kCLLocationAccuracyThreeKilometers
#define RW_LOCATION_DISTANCE_FILTER 1000

#define RW_REQUEST_TIMEOUT 15

// GUI
#define RW_FUNCTIONALCELL_DEF_HEIGHT 44.

#endif
