//
//  RWProfileView.m
//  reviewit
//
//  Created by Peter Gusev on 9/11/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "RWProfileView.h"
#import "UIColor+RWThemeColors.h"
#import "PTNArtifacts/PTNLogger.h"
#import "RWStorage.h"

#define RW_DESCRIPTION_LENGTH 70

@implementation RWProfileView
@synthesize delegate;
@synthesize businessCategoryLabel;
@synthesize businessTitleLabel;
@synthesize businessWebsiteButton;
@synthesize businessCategoryImageView;
@synthesize businessImageView;
@synthesize businessAddressButton;
@synthesize businessPhoneButton;
@synthesize reviewsNumLabel, reviewsTextLabel;

//********************************************************************
#pragma mark - received actions
- (IBAction)postReviewPressed:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(profileViewPostReviewPressed:)])
        [self.delegate profileViewPostReviewPressed:self];
}

- (IBAction)businessAddressPressed:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(profileViewBusinessAddressPressed:)])
        [self.delegate profileViewBusinessAddressPressed:self];
}

- (IBAction)businessPhonePressed:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(profileViewBusinessPhonePressed:)])
        [self.delegate profileViewBusinessPhonePressed:self];
}

- (IBAction)businessWebsitePressed:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(profileViewBusinessWebsitePressed:)])
        [self.delegate profileViewBusinessWebsitePressed:self];
}

//********************************************************************
#pragma mark - initialization and memory management
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self initialize];        
    }
    return self;
}

-(void)initialize
{
    
}

-(void)dealloc
{
    self.delegate = nil;
    self.businessAddressButton = nil;
    self.businessCategoryImageView = nil;
    self.businessCategoryLabel = nil;
    self.businessImageView = nil;
    self.businessPhoneButton = nil;
    self.businessTitleLabel = nil;
    self.businessWebsiteButton = nil;
    self.reviewsNumLabel = nil;
    self.reviewsTextLabel = nil;
    self.businessDescriptionLabel = nil;
}

-(void)awakeFromNib
{
    self.businessDescriptionLabel.delegate = self;
    self.businessDescriptionLabel.visibleCharactersNum = RW_DESCRIPTION_LENGTH;
    self.businessDescriptionLabel.functionColor = [UIColor rwDarkBlueThemeColor];
    self.businessDescriptionLabel.functionHighlightedColor = [UIColor rwLightBlueThemeColor];
    
	NSAttributedString* attrStr = [[RWStorage sharedStorageController] getBubblePromoString];

    self.promoBubbleLabel.automaticallyAddLinksForType = 0;
	self.promoBubbleLabel.attributedText = attrStr;
    self.promoBubbleLabel.centerVertically = YES;
    RWStorage *storage = [RWStorage sharedStorageController];
    
	[self.promoBubbleLabel addCustomLink:[storage getBubbleURL]
                                 inRange:[attrStr.string rangeOfString:[storage getBubbleLinkText]]];
}
//********************************************************************
#pragma mark - delegation: PTNExpandableLabel
-(void)expandableLabel:(PTNExpandableLabel *)expandableLabel willExpandToNewSize:(CGSize)newLabelSize duration:(NSTimeInterval)duration
{
    [UIView animateWithDuration:duration
                     animations:^(){
                         float verticalOffset = (newLabelSize.height-expandableLabel.frame.size.height);
                                                verticalOffset = (verticalOffset<0)?0:verticalOffset;
                         CGRect newFrame = self.descriptionContainerView.frame;
                         newFrame.size.height += verticalOffset;
                         
                         self.descriptionContainerView.frame = newFrame;
                         
                         newFrame = self.postReviewContainerView.frame;
                         newFrame.origin.y += verticalOffset;
                         self.postReviewContainerView.frame = newFrame;
                         
                         newFrame = self.frame;
                         newFrame.size.height += verticalOffset;
                         
                         if (self.delegate && [self.delegate respondsToSelector:@selector(profileView:descriptionExpandedWithAnimationDuration:forNewSize:)])
                             [self.delegate profileView:self descriptionExpandedWithAnimationDuration:duration forNewSize:newFrame.size];
                         
                         self.frame = newFrame;
                     }
                     completion:^(BOOL finished){
                         if (self.delegate && [self.delegate respondsToSelector:@selector(profileViewDidFinishedDescriptionExpansion:)])
                             [self.delegate profileViewDidFinishedDescriptionExpansion:self];
                     }];
}

-(void)expandableLabelDidAnimationFinished:(PTNExpandableLabel *)expandableLabel
{
    
}

//********************************************************************
#pragma mark - delegation: OHAttributedLabel
-(BOOL)attributedLabel:(OHAttributedLabel*)attributedLabel shouldFollowLink:(NSTextCheckingResult*)linkInfo
{
    return YES;
}
//********************************************************************
#pragma mark - protocol: <#protocol name#>

//********************************************************************
#pragma mark - notifications
// <#your notification's handlers' code goes here#>

//********************************************************************
#pragma mark - overriden
// <#your overide code goes here#>

//********************************************************************
#pragma mark - class methods
// <#your class methods code goes here#>

//********************************************************************
#pragma mark - properties
// <#your customized accessors' code goes here#>

//********************************************************************
#pragma mark - public methods
// <#your public methods' code goes here#>

//********************************************************************
#pragma mark - private methods
// <#your private methods' code goes here#>

@end
