//
//  RWProfileView.h
//  reviewit
//
//  Created by Peter Gusev on 9/11/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

/**
 Handles the View part for an RWProfileCell. In other words, it handles/fills the NIB for the
 business profile.
 */

#import <UIKit/UIKit.h>
#import "PTNArtifacts/PTNControls.h"
#import <OHAttributedLabel/OHAttributedLabel.h>

@interface RWProfileView : UIView
<PTNExpandableLabelDelegate, OHAttributedLabelDelegate>

@property (weak, nonatomic) IBOutlet id delegate;
@property (weak, nonatomic) IBOutlet UILabel *businessTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *businessWebsiteButton;
@property (weak, nonatomic) IBOutlet UILabel *businessCategoryLabel;
@property (weak, nonatomic) IBOutlet UIImageView *businessImageView;
@property (weak, nonatomic) IBOutlet UIImageView *businessCategoryImageView;
@property (weak, nonatomic) IBOutlet UIButton *businessAddressButton;
@property (weak, nonatomic) IBOutlet UIButton *businessPhoneButton;
@property (weak, nonatomic) IBOutlet UILabel *reviewsNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *reviewsTextLabel;
@property (weak, nonatomic) IBOutlet PTNExpandableLabel *businessDescriptionLabel;
@property (weak, nonatomic) IBOutlet OHAttributedLabel *promoBubbleLabel;

@property (weak, nonatomic) IBOutlet UIView *descriptionContainerView;

@property (weak, nonatomic) IBOutlet UIView *postReviewContainerView;

- (IBAction)postReviewPressed:(id)sender;
- (IBAction)businessAddressPressed:(id)sender;
- (IBAction)businessPhonePressed:(id)sender;
- (IBAction)businessWebsitePressed:(id)sender;

@end

@protocol RWProfileViewDelegate <NSObject>

@optional
-(void)profileViewPostReviewPressed:(RWProfileView*)profileView;
-(void)profileViewBusinessAddressPressed:(RWProfileView*)profileView;
-(void)profileViewBusinessPhonePressed:(RWProfileView*)profileView;
-(void)profileViewBusinessWebsitePressed:(RWProfileView*)profileView;
-(void)profileView:(RWProfileView*)profileView descriptionExpandedWithAnimationDuration:(NSTimeInterval)duration forNewSize:(CGSize)newSize;
-(void)profileViewDidFinishedDescriptionExpansion:(RWProfileView*)profileView;

@end