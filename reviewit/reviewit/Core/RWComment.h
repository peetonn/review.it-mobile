//
//  RWComment.h
//  reviewit
//
//  Created by Peter Gusev on 10/21/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "RWBaseObject.h"
#import "RWBusinessBrief.h"
#import "RWUser.h"

@interface RWComment : RWBaseObject

/**
 * @name Properties
 */
/**
 * User
 */
@property (nonatomic, retain) RWUser *user;
/**
 * Message
 */
@property (nonatomic, retain) NSString *message;
/**
 * Date of creation
 */
@property (nonatomic, readonly) NSString *createdString;

/**
 * GUI properties - was viewed or not
 */
@property (nonatomic) BOOL wasViewed;
/**
 * GUI properties - view height
 */
@property (nonatomic) CGFloat viewHeight;

/**
 * @name Class methods
 */
+(void)reviewsForBusiness:(RWBusinessBrief*)business limit:(NSUInteger)limit page:(NSUInteger)page callback:(RWObjectArrayLoadingCallback)block;


/**
 * @name Instance methods
 */
/**
 * Loads user's data from server
 */
-(void)loadUserData:(RWObjectLoadingCallback)onLoadFinished;

@end
