//
//  RWSearchViewController.h
//  reviewit
//
//  Created by Peter Gusev on 7/13/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

/**
 Represents the first screen that's available to the user. Accepts input
 and kicks off the search, which is then further handled by the class
 RWResultsViewController.
 */

#import <UIKit/UIKit.h>
#import "RWLocationViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <QuartzCore/QuartzCore.h>
#import "RWSuperViewController.h"
#import "RWResultsViewController.h"

@interface RWSearchViewController : RWSuperViewController
<RWChoseLocationDelegate, UITextFieldDelegate,
CLLocationManagerDelegate, RWResultsViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UITextField *searchBox;
@property (weak, nonatomic) IBOutlet UIButton *locationButton;

@property (strong, nonatomic) CLLocationManager *locationManager;
@property (nonatomic) RWLocation *selectedLocation;
@property (nonatomic) NSString *searchKeyword;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *locatingIndicator;
@property (weak, nonatomic) IBOutlet UIView *sboxLogoView;
@property (weak, nonatomic) IBOutlet UIView *sboxView;
@property (weak, nonatomic) IBOutlet UIView *locationView;

@end
