//
//  RWCommentView.m
//  reviewit
//
//  Created by Peter Gusev on 10/21/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "RWCommentView.h"
#import "UIColor+RWThemeColors.h"
#import <Quartzcore/QuartzCore.h>
#import "PTNArtifacts/PTNLogger.h"

#define RW_REVIEW_VISIBLE_LENGTH 120
#define RW_REVIEW_IMAGE_CORNER_RADIUS 4

@implementation RWCommentView

//********************************************************************
#pragma mark - received actions
// <#your IBAction's handlers goes here#>

//********************************************************************
#pragma mark - initialization and memory management
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self initialize];
    }
    return self;
}

-(void)initialize
{
    
}

-(void)dealloc
{
    self.delegate = nil;
    self.messageLabel = nil;
    self.userLogoImageView = nil;
    self.userNameLabel = nil;
    self.dateLabel = nil;
    self.infoAreaView = nil;
}

-(void)awakeFromNib
{
    self.messageLabel.delegate = self;
    self.messageLabel.visibleCharactersNum = RW_REVIEW_VISIBLE_LENGTH;
    self.messageLabel.functionColor = [UIColor rwDarkBlueThemeColor];
    self.messageLabel.functionHighlightedColor = [UIColor rwLightBlueThemeColor];
    self.messageLabel.expansionAnimationDuration = 0.3;
    self.userLogoImageView.layer.cornerRadius = RW_REVIEW_IMAGE_CORNER_RADIUS;
    self.userLogoImageView.clipsToBounds = YES;
}

//********************************************************************
#pragma mark - delegation: PTNExpandableLabel
-(void)expandableLabel:(PTNExpandableLabel *)expandableLabel willExpandToNewSize:(CGSize)newLabelSize duration:(NSTimeInterval)duration
{
    __block float verticalOffset = (newLabelSize.height-expandableLabel.frame.size.height);
    
    if (verticalOffset != 0 && self.frame.size.height+verticalOffset >= RW_REVIEW_MIN_HEIGHT)
    {
    [UIView animateWithDuration:duration
                     animations:^(){
                         
                         CGRect newFrame = self.frame;
                         newFrame.size.height += verticalOffset;
                         
                         if (self.delegate && [self.delegate respondsToSelector:@selector(commentView:messageExpandedWithAnimationDuration:forNewSize:)])
                             [self.delegate commentView:self
                   messageExpandedWithAnimationDuration:duration
                                             forNewSize:newFrame.size];
                         self.frame = newFrame;
                     }
                     completion:^(BOOL finished){
                         if (self.delegate && [self.delegate respondsToSelector:@selector(commentViewDidFinishedMessageExpansion:)])
                             [self.delegate commentViewDidFinishedMessageExpansion:self];
                     }];
    }
}
-(void)expandableLabelDidAnimationFinished:(PTNExpandableLabel *)expandableLabel
{
    
}

//********************************************************************
#pragma mark - protocol: <#protocol name#>
// <#your conformed protocol code goes here#>

//********************************************************************
#pragma mark - notifications
// <#your notification's handlers' code goes here#>

//********************************************************************
#pragma mark - overriden

//********************************************************************
#pragma mark - class methods
// <#your class methods code goes here#>

//********************************************************************
#pragma mark - properties
// <#your customized accessors' code goes here#>

//********************************************************************
#pragma mark - public methods
// <#your public methods' code goes here#>

//********************************************************************
#pragma mark - private methods
// <#your private methods' code goes here#>

@end
