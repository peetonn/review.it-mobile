//
//  RWDesignController.h
//  reviewit
//
//  Created by Peter Gusev on 8/28/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

/**
 Customizes the user interface using the Appearance API.
 */

#import <Foundation/Foundation.h>
#import "UIColor+RWThemeColors.h"

@interface RWDesignController : NSObject

+(void)setupDesign;
+(UIBarButtonItem*)defaultBackButtonWithTarget:(id)target action:(SEL)selector;
+(UIBarButtonItem*)rightButtonWithTitle:(NSString*)title target:(id)target action:(SEL)selector;
+(void)setNavigationBar:(UINavigationBar*)navigationBar styleShadowed:(BOOL)isShadowed;
+(void)setSearchBar:(UISearchBar*)searchBar styleShadowed:(BOOL)isShadowed;

@end
