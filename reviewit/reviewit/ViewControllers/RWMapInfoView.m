//
//  RWMapInfoView.m
//  reviewit
//
//  Created by Peter Gusev on 12/3/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "RWMapInfoView.h"

@implementation RWMapInfoView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)setInfoStateForSearchResult:(RWSearchResult)state
{
    switch (state) {
        case RWSearchResultEmpty:
        {
            self.infoLabel.text = NSLocalizedStringFromTable(@"No results text short", @"RWLocalized", @"");
            [self.loadingIndicator stopAnimating];
        }
            break;
        case RWSearchResultError:
        {
            self.infoLabel.text = NSLocalizedStringFromTable(@"Search results error text short", @"RWLocalized", @"");
            [self.loadingIndicator stopAnimating];
        }
            break;
        case RWSearchResultLoading:
        {
            self.infoLabel.text = NSLocalizedStringFromTable(@"Loading progress text", @"RWLocalized", @"");
            [self.loadingIndicator startAnimating];
        }
            break;
        default:
        {
            self.infoLabel.text = @"";
            [self.loadingIndicator stopAnimating];
        }
            break;
    }
}

@end
