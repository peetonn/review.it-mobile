//
//  main.m
//  reviewit
//
//  Created by Peter Gusev on 7/2/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"
#import "RWStorage.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        int result = 0;
        @try {
            result = UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
        }
        @catch (NSException *exception) {
            LOG_ERROR(@"Caught exception: %@ (description: %@) with callstack: \n%@", [exception name], [exception description], [exception callStackSymbols]);
        }
        @finally {
            return result;
        }
        return result;
    }
}
