//
//  RWDesignController.m
//  reviewit
//
//  Created by Peter Gusev on 8/28/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "RWDesignController.h"
#import <OHAttributedLabel/OHAttributedLabel.h>
#import <QuartzCore/QuartzCore.h>

@implementation RWDesignController

+(void)setupDesign
{
    [RWDesignController setupNavigationBarDesign];
    [RWDesignController setupSearchBarDesign];
    [RWDesignController setupOHAttributedLabel];
}

+(void)setNavigationBar:(UINavigationBar*)navigationBar styleShadowed:(BOOL)isShadowed
{
    if (isShadowed)
    {
        [RWDesignController applyRWStyleShadowForView:navigationBar];
        [navigationBar setBackgroundImage:[UIImage imageNamed:@"navbar2.png"] forBarMetrics:UIBarMetricsDefault];
    }
    else
    {
        [RWDesignController resetRWStyleShadowForView:navigationBar];
        [navigationBar setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    }
}

+(void)setSearchBar:(UISearchBar*)searchBar styleShadowed:(BOOL)isShadowed
{
    if (isShadowed)
    {
        [RWDesignController applyRWStyleShadowForView:searchBar];
    }
    else
    {
        [RWDesignController resetRWStyleShadowForView:searchBar];
    }
}

+(void)setupSearchBarDesign
{
    [[UISearchBar appearance] setImage:[UIImage imageNamed:@"sbar_sicon.png"] forSearchBarIcon:UISearchBarIconSearch state:UIControlStateNormal];
    [[UISearchBar appearance] setSearchFieldBackgroundImage:[UIImage imageNamed:@"sbar_textfield.png"] forState:UIControlStateNormal];
    [[UISearchBar appearance] setBackgroundImage:[UIImage imageNamed:@"searchbar.png"]];
}

+(void)setupNavigationBarDesign
{
    NSDictionary *navBarTitleFontAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                               [UIFont fontWithName:@"American Typewriter" size:22.], UITextAttributeFont,
                                               [UIColor clearColor], UITextAttributeTextShadowColor,
                                               nil];
    UIOffset offset = UIOffsetMake(.5, .5);
    NSNumber *shadowOffset = [[NSNumber alloc] initWithBytes:&offset objCType:@encode(UIOffset)];
    
    [[UINavigationBar appearance] setTitleTextAttributes:navBarTitleFontAttributes];    
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"navbar.png"] forBarMetrics:UIBarMetricsDefault];
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{
                                     UITextAttributeFont:[UIFont boldSystemFontOfSize:14.],
                                UITextAttributeTextColor:[UIColor whiteColor],
                          UITextAttributeTextShadowColor:[UIColor rwGrayThemeColor],
                         UITextAttributeTextShadowOffset:shadowOffset}
                                                forState:UIControlStateNormal];
    [[UIBarButtonItem appearance] setTitleTextAttributes:@{
                                     UITextAttributeFont:[UIFont boldSystemFontOfSize:14.],
                                UITextAttributeTextColor:[UIColor lightGrayColor],
                          UITextAttributeTextShadowColor:[UIColor rwGrayThemeColor],
                         UITextAttributeTextShadowOffset:shadowOffset}
                                                forState:UIControlStateDisabled];
    
    UIImage *i = [[UIImage imageNamed:@"navbarback.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 30, -1, 10)];
    [[UIBarButtonItem appearance] setBackButtonBackgroundImage:i forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(-6, 0) forBarMetrics:UIBarMetricsDefault];
    
    UIImage *i2 = [[UIImage imageNamed:@"navbarbutton.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    [[UIBarButtonItem appearance] setBackgroundImage:i2 forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
}

+(UIBarButtonItem*)defaultBackButtonWithTarget:(id)target action:(SEL)selector
{
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle:NSLocalizedStringFromTable(@"Back button text",@"RWLocalized", @"")
                                   style:UIBarButtonItemStyleDone
                                   target:target
                                   action:selector];
    
    UIImage *i = [[UIImage imageNamed:@"navbarback.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 30, -1, 10)];
    
    [backButton setBackgroundImage:i
                          forState:UIControlStateNormal
                        barMetrics:UIBarMetricsDefault];
    return backButton;
}

+(UIBarButtonItem*)rightButtonWithTitle:(NSString*)title target:(id)target action:(SEL)selector
{
    UIBarButtonItem *btn = [[UIBarButtonItem alloc] initWithTitle:title
                                                            style:UIBarButtonItemStyleDone
                                                           target:target
                                                           action:selector];
    
    return btn;
}

+(void)setupOHAttributedLabel
{

    // setup link color
    [[OHAttributedLabel appearance] setLinkColor:[UIColor rwWhiteThemeColor]];

    // setup link style
    [[OHAttributedLabel appearance] setLinkUnderlineStyle:kCTUnderlineStyleNone|kOHBoldStyleTraitSetBold];
}

+(void)applyRWStyleShadowForView:(UIView*)view
{
    // add the drop shadow
    view.layer.shadowColor = [[UIColor blackColor] CGColor];
    view.layer.shadowOffset = CGSizeMake(0.0, 0.5);
    view.layer.shadowOpacity = 0.25;
    view.layer.masksToBounds = NO;
}

+(void)resetRWStyleShadowForView:(UIView*)view
{
    view.layer.shadowColor = nil;
    view.layer.shadowRadius = 0.;
    view.layer.shadowOffset = CGSizeMake(0., 0.);
    view.layer.shadowOpacity = 0.;
    view.layer.masksToBounds = YES;
}

@end
