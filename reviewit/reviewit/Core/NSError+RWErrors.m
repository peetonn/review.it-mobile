//
//  NSError+RWErrors.m
//  reviewit
//
//  Created by Peter Gusev on 12/3/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "NSError+RWErrors.h"

#define RW_ERROR_DOMAIN @"it.review.errors"

NSInteger const RWErrorBadArgument = 1;

@implementation NSError (RWErrors)

+(NSError*)errorWithCode:(NSInteger)code message:(NSString*)msg
{
    return [NSError errorWithDomain:RW_ERROR_DOMAIN code:code userInfo:@{NSLocalizedDescriptionKey:msg}];
}

@end
