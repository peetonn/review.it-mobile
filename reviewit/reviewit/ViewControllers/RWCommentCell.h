//
//  RWCommentCell.h
//  reviewit
//
//  Created by Peter Gusev on 10/21/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

/**
 Represents a single position in a list of comments. Uses the RWCommentView for its
 user interface needs.
 */

#import <UIKit/UIKit.h>
#import "RWComment.h"
#import "RWCommentView.h"

@interface RWCommentCell : UITableViewCell
<RWCommentViewDelegate>
{
    __weak RWComment *_review;
}

@property (nonatomic, weak) IBOutlet id delegate;
@property (nonatomic, weak) IBOutlet RWCommentView *commentView;

@property (nonatomic, weak) RWComment *review;

@end

@protocol RWCommentCellDelegate <NSObject>

-(void)commentCell:(RWCommentCell*)cell willChangeSize:(CGSize)newSize;
-(void)commentCellDidChangeSize:(RWCommentCell*)cell;

@end
