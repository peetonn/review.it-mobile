//
//  RWMapResultsViewController.m
//  reviewit
//
//  Created by Peter Gusev on 8/1/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "RWMapResultsViewController.h"
#import "RWResultsViewController.h"
#import "RWBusinessBrief.h"
#import "RWAnnotationView.h"
#import "RWProfileViewController.h"
#import "RWBusinessBrief.h"
#import "PTNArtifacts/PTNArtifacts.h"

#define RW_MAPQUEUE_ID "it.review.mapqueue"
#define RW_MAPVIEW_ANIMATION_DURATION 0.2
#define RW_MAPVIEW_INFO_TIME 3.
//#undef PTN_LOG_TRACE

@interface RWMapResultsViewController ()
//@property (nonatomic) RWSearchResult searchResultState;
@property (nonatomic) BOOL allowSearchRedo;
@property (nonatomic) BOOL changingMapViewProgrammatically;
@property (nonatomic, weak) id<MKAnnotation> selectedAnnotation;
@property (nonatomic) float currentSearchRadius;
@property (nonatomic, strong) CLLocation *currentSearchCenter;
@property (nonatomic) RWSearchResult searchResultState;
@end

@implementation RWMapResultsViewController
@synthesize selectedAnnotation, searchResultState = _searchResultState;

//********************************************************************
#pragma mark - received actions
-(IBAction)searchNewAreaButtonTapped:(id)sender
{
    [self updateCenterAndRadiusWihtCurrentMapViewRegion];
    [self setSearchButtonViewVisible:NO];
    [self loadResultsWithinRadius:self.currentSearchRadius];
}

//********************************************************************
#pragma mark - initialization and memory management
@synthesize mapView;
-(void)initialize
{
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        [self initialize];
    }
    return self;
}
-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self initialize];
    }
    return self;
}
-(void)dealloc
{
}
//********************************************************************
#pragma mark - view lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.allowSearchRedo = NO;
    self.changingMapViewProgrammatically = YES;
    self.currentSearchRadius = [[RWStorage sharedStorageController] locationSearchRadius];
    self.currentSearchCenter = self.searchResultsDataController.searchLocation.location;
    [self updateMapViewRegionWithCurrentCenterAndRadius];
    
    [UIView setPopupAnimationDuration:RW_MAPVIEW_ANIMATION_DURATION];
    
}
- (void)viewDidUnload
{
    [self setMapView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"LoadProfileFromMap"])
    {
        RWProfileViewController *vc = (RWProfileViewController*)segue.destinationViewController;
        vc.business = self.selectedAnnotation;
    }
}
//********************************************************************
#pragma mark - delegation: MKMapView delegate
-(MKAnnotationView*)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    static NSString *AnnotationViewID = @"annotationViewID";
    
    RWAnnotationView *annotationView = (RWAnnotationView*)[self.mapView dequeueReusableAnnotationViewWithIdentifier:AnnotationViewID];
    
    if (annotationView == nil)
    {
        annotationView = [[RWAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:AnnotationViewID];
    }
    else {
        annotationView.annotation = annotation;            
    }
    
    [annotationView setEnabled:YES];
    [annotationView setCanShowCallout:YES];

    return annotationView;
}

-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    [self performSegueWithIdentifier:@"LoadProfileFromMap" sender:self];
}

-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    self.selectedAnnotation = view.annotation;
}

-(void)mapView:(MKMapView *)aMapView regionDidChangeAnimated:(BOOL)animated
{
    if (self.allowSearchRedo)
    {
        if (!self.changingMapViewProgrammatically)
            [self setSearchButtonViewVisible:YES];
        else
            self.changingMapViewProgrammatically = NO;
    }
}
//********************************************************************
#pragma mark - protocol: RWSearchResultsContentProtocol
-(id<RWSearchResultsDataController>)searchResultsDataController
{
    return (id<RWSearchResultsDataController>)self.parentViewController;
}
-(void)initiateSearch
{
    [self updateCenterAndRadiusWihtCurrentMapViewRegion];
    self.allowSearchRedo = YES;
    [self loadResultsWithinRadius:self.currentSearchRadius];
}
-(void)didCurrentSearchResultsStateChanged:(RWSearchResult)newResult
{
    self.allowSearchRedo = YES;
    switch (newResult) {
        case RWSearchResultOK:
        {
            [self retrieveLocationsFromSearchResults:^(NSArray *locations){
                [self updateResultsRegionWithLocations:locations];
                if ([locations count])
                {
                    self.searchResultState = RWSearchResultOK;
                    self.changingMapViewProgrammatically = YES;
                    self.mapView.region = [self getSearchResultsRegionForLocations:locations];
                }
                else
                {
                    LOG_WARN(@"No location data for businesses");
                    self.searchResultState = RWSearchResultEmpty;
                }
            }];
        }break;
        default:
            self.searchResultState = newResult;
            break;
    }
}

//********************************************************************
#pragma mark - notifications


//********************************************************************
#pragma mark - overriden


//********************************************************************
#pragma mark - class methods
// 

//********************************************************************
#pragma mark - properties
-(RWSearchResult)searchResultState
{
    return _searchResultState;
}
-(void)setSearchResultState:(RWSearchResult)newResultsState
{
    _searchResultState = newResultsState;
    [self setInfoViewVisible:(newResultsState != RWSearchResultOK) state:newResultsState];
}
-(NSArray*)searchResults
{
    return self.searchResultsDataController.searchResults;
}

//********************************************************************
#pragma mark - public methods
-(void)updateResultsRegionWithLocations:(NSArray*)locations
{
    if ([self.mapView.annotations count] > 0)
        [self.mapView removeAnnotations:self.mapView.annotations];
    
    if (locations)
    {
        // adding all search results, but updating map only for locations provided
        [self.mapView addAnnotations:self.searchResultsDataController.searchResults];
    
        if ([locations count])
        {
            self.changingMapViewProgrammatically = YES;
            self.selectedAnnotation = [locations objectAtIndex:0];
            [self.mapView selectAnnotation:self.selectedAnnotation animated:YES];
            self.changingMapViewProgrammatically = NO;
        }
    }
}

//********************************************************************
#pragma mark - private methods
-(void)setSearchButtonViewVisible:(BOOL)isVisible
{
    [self.view setSlideView:self.searchButtonView
                    visible:isVisible
                   animated:YES
              alignmentMask:PTNPopupAlignmentMaskBottom|PTNPopupAlignmentMaskCenter
                  slideMask:PTNSlideDirectionMaskVertical
           animationOptions:UIViewAnimationCurveEaseInOut
           animationBlock:nil
         completionCallback:nil];
}
-(void)setInfoViewVisible:(BOOL)isVisible state:(RWSearchResult)state
{
    [self.infoView setInfoStateForSearchResult:state];
    [self.view setSlideView:self.infoView
                    visible:isVisible
                   animated:YES
              alignmentMask:PTNPopupAlignmentMaskTop|PTNPopupAlignmentMaskCenter
                  slideMask:PTNSlideDirectionMaskVertical
           animationOptions:UIViewAnimationCurveEaseInOut
	        animationBlock:nil           
         completionCallback:nil];
}
// some businesses don't have exact coordinates, so they need to be geocoded
// for each received business we're going to check if geocoding needed and make it if so
// upon all geocoding completes, call user's callback (in the most nested block)
-(void)retrieveLocationsFromSearchResults:(void (^)(NSArray*))block
{
    NSMutableArray *locations = [NSMutableArray array];
    NSMutableArray *geocodingRequired = [NSMutableArray array];
    
    for (RWBusinessBrief *business in self.searchResults)
    {
        if (business.isGeocodingNeeded && business.canGeocode)
            [geocodingRequired addObject:business];
        else
            [locations addObject:business];
    }
    
    if ([geocodingRequired count])
    {
        __block int nBusinesses = [geocodingRequired count];
        for (RWBusinessBrief *business in geocodingRequired)
        {
            [business geocodeLocationCallback:^(RWBaseObject *bis, NSError *err){
                --nBusinesses;
                // if successfully geocoded - add to array
                if (!err)
                    [locations addObject:bis];                
                if (!nBusinesses && block)
                    block(locations);
            }];
        }
    }
    else if (block)
            block(locations);
}

-(MKCoordinateRegion)getSearchResultsRegionForLocations:(NSArray*)locations
{
    __block float minLat = 90, minLon = 180, maxLat = -90, maxLon = -180;
    
    [locations enumerateObjectsUsingBlock:^(RWBusinessBrief *business, NSUInteger idx, BOOL *stop){
        if (business.details.latitude < minLat)
            minLat = business.details.latitude;
        if (business.details.latitude > maxLat)
            maxLat = business.details.latitude;
        if (business.details.longitude < minLon)
            minLon = business.details.longitude;
        if (business.details.longitude > maxLon)
            maxLon = business.details.longitude;
    }];
    
    CLLocationCoordinate2D center =  CLLocationCoordinate2DMake((maxLat+minLat)/2, (maxLon+minLon)/2);
    MKCoordinateSpan span = MKCoordinateSpanMake(fabs(maxLat-minLat),  fabs(maxLon-minLon));

    if (span.latitudeDelta < 180)
        span.latitudeDelta *= 1.05;
    if (span.longitudeDelta < 360)
        span.longitudeDelta *= 1.05;
    
    return MKCoordinateRegionMake(center, span);
}

// show map for results
// filter results for area
-(void)loadResultsWithinRadius:(float)radius
{
    self.searchResultState = RWSearchResultLoading;
    [self setSearchButtonViewVisible:NO];
    [self.searchResultsDataController searchResultsContentViewControllerWillStartSearch:self];
    [RWBusinessBrief businessBriefsForKeyword:self.searchResultsDataController.searchKeyword
                                 nearLocation:self.currentSearchCenter
                                   withRadius:radius
                              completionBlock:^(NSArray *businesses, NSError *err){
                                  [self.searchResultsDataController searchResultsContentViewControllerDidFinishSearch:self];
                                  if (err)
                                  {
                                      LOG_ERROR(@"error while making location-based request: %@",err);
                                      self.searchResultState = RWSearchResultError;
                                  }
                                  else
                                      if (!businesses.count)
                                          self.searchResultState = RWSearchResultEmpty;
                                      else
                                          self.searchResultState = RWSearchResultOK;
                                  
                                  [self.searchResultsDataController searchResultsContentViewController:self
                                                                                    providedNewResults:businesses
                                                                                      withSearchResult:self.searchResultState];
                                  [self updateResultsRegionWithLocations:businesses];

                              }];
}
-(void)updateCenterAndRadiusWihtCurrentMapViewRegion
{
    MKMapPoint eastMapPoint = MKMapPointMake(MKMapRectGetMinX(self.mapView.visibleMapRect), MKMapRectGetMidY(self.mapView.visibleMapRect));
    MKMapPoint westMapPoint = MKMapPointMake(MKMapRectGetMaxX(self.mapView.visibleMapRect), MKMapRectGetMidY(self.mapView.visibleMapRect));
    self.currentSearchRadius = MKMetersBetweenMapPoints(eastMapPoint, westMapPoint)/2.;
    self.currentSearchCenter = [[CLLocation alloc] initWithLatitude: self.mapView.region.center.latitude
                                                          longitude: self.mapView.region.center.longitude];
    
}
-(void)updateMapViewRegionWithCurrentCenterAndRadius
{
    [self.mapView setRegion:MKCoordinateRegionMakeWithDistance(self.currentSearchCenter.coordinate, 2.*self.currentSearchRadius, 2.*self.currentSearchRadius)];    
}
@end
