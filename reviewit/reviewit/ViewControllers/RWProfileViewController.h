//
//  RWProfileViewController.h
//  reviewit
//
//  Created by Peter Gusev on 7/13/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

/**
 Code to handle the upper part of the profile screen. Uses the RWProfileView and the
 RWCommentView to 
 */

#import <UIKit/UIKit.h>
#import "RWBusinessBrief.h"
#import "RWSuperViewController.h"
#import "RWProfileView.h"
#import "RWCommentView.h"
#import "RWCommentCell.h"
#import "RWPaginatedViewController.h"

@interface RWProfileViewController : RWPaginatedViewController
<RWProfileViewDelegate, RWCommentCellDelegate>
{
    __strong RWBusinessBrief *_business;
}

@property (nonatomic, strong) RWBusinessBrief *business;

@end
