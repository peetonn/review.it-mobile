//
//  RWResultsInformationViewController.h
//  reviewit
//
//  Created by Peter Gusev on 8/16/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

/**
 Displays the searching status (such as loading, error etc) 
 before showing the results.
 */

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <OHAttributedLabel/OHAttributedLabel.h>
#import "RWResultsControllerProtocols.h"

typedef enum _RWInformationViewState {
    RWEmptyResultsState,
    RWErrorState,
    RWLoadingState
} RWInformationViewState;

@protocol RWResultsInformationViewControllerDelegate;

@interface RWResultsInformationViewController : UIViewController
<OHAttributedLabelDelegate>
{
    RWInformationViewState _state;
}

@property (readonly) RWInformationViewState state;

@property (weak, nonatomic) id<RWResultsInformationViewControllerDelegate> delegate;

@property (strong, nonatomic) IBOutlet UIView *loadingView;
@property (strong, nonatomic) IBOutlet UIView *informationView;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *loadingIndicator;
@property (weak, nonatomic) IBOutlet OHAttributedLabel *informationLabel;

-(void)setLoadingState;
-(void)setErrorState;
-(void)setEmptyResultsState;
-(void)setStateForSearchResultState:(RWSearchResult)state;

@end

@protocol RWResultsInformationViewControllerDelegate <NSObject>

@optional
-(void)resultsInformationViewControllerDidTappedTryAgainLink:(RWResultsInformationViewController*)vc;

@end
