//
//  RWSimpleCell.h
//  reviewit
//
//  Created by Peter Gusev on 8/28/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RWSimpleCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *textLabel;

@end
