//
//  RWFunctionCell.m
//  reviewit
//
//  Created by Peter Gusev on 10/23/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "RWFunctionCell.h"

@implementation RWFunctionCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _isLoadingState = NO;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureViewForPath:(NSIndexPath *)indexPath
{
    [self.loadingIndicator startAnimating];
}

@end
