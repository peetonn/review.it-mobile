//
//  reviewitAppTests.m
//  reviewitAppTests
//
//  Created by Peter Gusev on 7/2/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "reviewitAppTests.h"
#import "AppDelegate.h"
#import "RWResultsInformationViewController.h"
#import "RWResultsViewController.h"
#import "RWListResultsViewController.h"
#import "RWMapResultsViewController.h"
#import "RWBusinessBrief.h"
#import "RWResultsViewController.h"

@interface RWResultsViewController ()

@property (nonatomic, strong) UIViewController<RWSearchResultsContentProtocol> *currentResultsViewController;
@property (nonatomic) BOOL isViewTransitionInProgress;

-(void)switchViewController:(UIViewController*)vc1 toViewController:(UIViewController*)vc2 withAnimation:(UIViewAnimationOptions)animation duration:(float)duration completionBlock:(void(^)(void))completionBlock;
-(void)loadMapResultsViewController;
-(void)loadListResultsViewController;
-(void)loadInformationViewController;
-(void)hideInformationViewController;

-(void)initiateSearch;
-(void)adjustCurrentViewToSuperview;
-(void)loadLastUsedResultsView;

-(void)setCoverViewHidden:(BOOL)hidden animated:(BOOL)animated;
-(void)setCancelButtonHidden:(BOOL)hidden animated:(BOOL)animated;
@end

@interface reviewitAppTests()

@property (nonatomic, weak) AppDelegate *rwdelegate;
@property (nonatomic, strong) RWResultsInformationViewController *infoVC;
@property (nonatomic, strong) RWResultsViewController *resultsVC;
@property (nonatomic, readonly) UIStoryboard *mainStoryboard;
@property (nonatomic, readonly) UIViewController *rootVC;

@end

@implementation reviewitAppTests

@synthesize rwdelegate, infoVC, resultsVC;

- (void)setUp
{
    [super setUp];
    
    self.rwdelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

-(void)testSearchResultsTableView
{
    self.resultsVC = [self.mainStoryboard instantiateViewControllerWithIdentifier:@"RWResultsViewController"];

    [self.rootVC presentViewController:[self embedInNavigationController:self.resultsVC] animated:NO completion:nil];
    [self waitFor:5];

    self.resultsVC.searchResults = [self fakeSearchResults];
    [self.resultsVC hideInformationViewController];
    
    [self waitFor:30];
}

- (void)testInfoViewTexts
{
    [self openInfoView];

    for (int i =0; i < 3; i++)
    {
        [self.infoVC setErrorState];
        [self waitFor:30];
        [self.infoVC setEmptyResultsState];
        [self waitFor:30];
    }
}

//********************************************************************************
-(void)initiateSearchStub
{
    self.resultsVC.searchKeyword = @"yoga";    
    [self.resultsVC hideInformationViewController];
    self.resultsVC.searchResults = [self fakeSearchResults];    
}
+(void)businessBriefsForKeyword:(NSString*)keyword andLocation:(RWLocation*)location completionBlock:(void(^)(NSArray *briefs, NSError *err))block
{
    NSLog(@"stub for loading search results");
}
-(UINavigationController*)embedInNavigationController:(UIViewController*)vc
{
    return [[UINavigationController alloc] initWithRootViewController:vc];
}
-(void)waitFor:(NSTimeInterval)seconds
{
    [[NSRunLoop mainRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:seconds]];
}
-(void)openInfoView
{
    self.infoVC = [[RWResultsInformationViewController alloc] init ];
    [[NSBundle mainBundle] loadNibNamed:@"RWResultsInformationView" owner:self.infoVC options:nil];
    self.infoVC.view.frame = self.rwdelegate.window.bounds;
    [self.rwdelegate.window addSubview:self.infoVC.view];
}
                    
-(UIStoryboard*)mainStoryboard
{
    return self.rwdelegate.window.rootViewController.storyboard;
}

-(NSArray*)fakeSearchResults
{
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    for (int i = 0; i < 12; i++)
    {    
        RWIndustry *cat = [[RWIndustry alloc] init];
        RWBusinessBrief *b = [[RWBusinessBrief alloc] init];
    
        b.name = [NSString stringWithFormat:@"Business %@", i+1];
        b.description = @"descriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescriptiondescription";
        cat.ID = 2;
        b.category.ID = i+1;
        b.category.name = [NSString stringWithFormat:@"category%@",i+1];
        b.reviewsNum = rand()%500;
        
        [arr addObject:b];
    }
    return arr;
}    

-(UIViewController*)rootVC
{
    return self.rwdelegate.window.rootViewController;
}

@end
