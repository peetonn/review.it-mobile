//
//  RWReviewsViewController.h
//  reviewit
//
//  Created by Peter Gusev on 7/19/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

/**
 Rsponsible for displaying the 
 */

#import <UIKit/UIKit.h>
#import "RWSuperViewController.h"
#import "RWBusinessBrief.h"
#import "RWResultsInformationViewController.h"

@protocol RWReviewsViewControllerDelegate;

/**
 * Class represents view controller for displaying reviews aka Facebook comments plugin
 */
@interface RWReviewsViewController : RWSuperViewController<UIWebViewDelegate>

/**
 * @name Properties
 */
/**
 * View controller's delegate which should conform to RWReviewsViewControllerDelegate protocol
 */
@property (weak, nonatomic) IBOutlet id<RWReviewsViewControllerDelegate> delegate;
/**
 * Instance of RWBusinessBrief reviews of which are displayed
 */
@property (weak, nonatomic) RWBusinessBrief *business;
/**
 * Web view for presenting Facebook comments plugin
 */
@property (weak, nonatomic) IBOutlet UIWebView *reviewsView;
/**
 * Indicates whether FB login is active
 */
@property (readonly) BOOL isLoggedIn;


/**
 * @name Instance methods
 */
/**
 * Initates process of loading reviews
 */
-(void)startLoadingReviews;
/**
 * Navigates back in web view if possible
 */
-(void)navigateBack;
/**
 * Determines if web view can navigate back
 */
-(BOOL)canNavigateBack;
/**
 * Reloads web view
 */
-(void)reloadPage;
/**
 * Log out from current FB session
 */
-(void)logout;

@end

/**
 * Protocol for delegates of REReviewsViewController
 */
@protocol RWReviewsViewControllerDelegate <NSObject>

@optional
/**
 * Called when web-page loading started
 */
-(void)reviewsViewControllerDidStartLoadingComments:(RWReviewsViewController*)aVC;
/**
 * Called whan web-page loading finished
 */
-(void)reviewsViewControllerDidFinishLoadingComments:(RWReviewsViewController*)aVC;
/**
 * Called for notifying delegate about occurred errors
 */
-(void)reviewsViewController:(RWReviewsViewController *)aVC didErroredLoadingComments:(NSError*)error;

@end
