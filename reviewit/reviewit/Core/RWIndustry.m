//
//  RWIndustry.m
//  reviewit
//
//  Created by Peter Gusev on 8/5/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "RWIndustry.h"
#import "PTNArtifacts/PTNAdditions.h"
#import "PTNArtifacts/PTNLogger.h"

#define RW_CI_BG        @"ci_pin_blue.png"
#define RW_CIMAP_BG     @"cimap_pin_white.png"
#define RW_CIMAP_BG_HL  @"cimap_pin_blue.png"

#define RW_CI_NORMAL_FORMAT         @"ci_%@.png"
#define RW_CIMAP_NORMAL_FORMAT      @"cimap_%@_hl.png"
#define RW_CIMAP_HIGHLIGHTED_FORMAT RW_CI_NORMAL_FORMAT
#define RW_CPH_FORMAT               @"cph_%@.png"

#define RW_CNAME_VILLA          @"villa"
#define RW_CNAME_TAXI           @"taxi"
#define RW_CNAME_YOGA           @"yoga"
#define RW_CNAME_CAFE           @"cafe"
#define RW_CNAME_RESTAURANT     @"restaurant"
#define RW_CNAME_ENTERTAIMENT   @"entertainment"
#define RW_CNAME_TRADESMAN      @"tradesman"
#define RW_CNAME_SCHOOL         @"school"
#define RW_CNAME_THERAPIST      @"therapist"
#define RW_CNAME_VISA           @"visa"

// parameters for placing cateogry icon above category pin image 
#define RW_CI_ICON_SCALE_FACTOR     .7
#define RW_CI_ICON_CENTERPOINT_X    .5
#define RW_CI_ICON_CENTERPOINT_Y    .4

// parameters for placing cateogry icon above category map pin image 
#define RW_CIMAP_ICON_SCALE_FACTOR     1.
#define RW_CIMAP_ICON_CENTERPOINT_X    .34
#define RW_CIMAP_ICON_CENTERPOINT_Y    .35  

#define RW_CNUM 10

// the map of correspondeces between category ID and category's local name
// (which is used for finding category-specific images)
typedef struct {
    const void *categoryKey;
    int categoryId;
} RWCategoryName_IDMapEntry;

const RWCategoryName_IDMapEntry catName_IDMap[RW_CNUM] = {
    {RW_CNAME_VILLA, 1},
    
    {RW_CNAME_TAXI, 2},
    
    {RW_CNAME_YOGA, 3},
    
    {RW_CNAME_VISA, 4},
    
    {RW_CNAME_THERAPIST, 5},
    
    {RW_CNAME_ENTERTAIMENT, 6},
    
    {RW_CNAME_SCHOOL, 7},
    
    {RW_CNAME_CAFE, 8},
    
    {RW_CNAME_RESTAURANT, 9},
    
    {RW_CNAME_TRADESMAN, 10}
};

static UIImage *catBackgroundImage = nil;
static UIImage *catMapBackgroundImage = nil;
static UIImage *catMapHighlightedBgImage = nil;

static NSMutableDictionary *cachedCategoryIcons = nil;
static NSMutableDictionary *cachedMapCategoryIcons = nil;
static NSMutableDictionary *cachedMapCategoryIconsHl = nil;

@interface  RWIndustry()

@property (nonatomic, readonly) NSString *internalCategoryName;

+(NSString*)getCategoryNameFromID:(int)ID;
+(UIImage*)getCatBgImage;
+(UIImage*)getCatMapBgImageHighlighted:(BOOL)isHighlighted;
@end 

@implementation RWIndustry
@synthesize status, name;

//********************************************************************
#pragma mark - received actions
// <#your IBAction's handlers goes here#>

//********************************************************************
#pragma mark - initialization and memory management
// <#your initialization&memory management code goes here#>

//********************************************************************
#pragma mark - delegation: <#sender#>
// <#your delegated methods goes here#>

//********************************************************************
#pragma mark - protocol: <#protocol name#>
// <#your conformed protocol code goes here#>

//********************************************************************
#pragma mark - notifications
// <#your notification's handlers' code goes here#>

//********************************************************************
#pragma mark - overriden


//********************************************************************
#pragma mark - class methods
+(NSString*)getCategoryNameFromID:(int)ID
{
    for (int i = 0; i < RW_CNUM; i++)
        if (catName_IDMap[i].categoryId == ID)
            return  (__bridge NSString*)catName_IDMap[i].categoryKey;
    return nil;
}
+(RKObjectMapping*)objectMapping
{
    RKObjectMapping *industryMapping = [RKObjectMapping mappingForClass:[RWIndustry class]];
    
    [industryMapping mapKeyPath:@"id" toAttribute:@"ID"];
    [industryMapping mapKeyPath:@"name" toAttribute:@"name"];
    [industryMapping mapKeyPath:@"status" toAttribute:@"status"];
    
    return industryMapping;
}
+(UIImage*)getCatBgImage
{
    if (!catBackgroundImage)
        catBackgroundImage = [UIImage imageNamed:RW_CI_BG];
    return catBackgroundImage;
}
+(UIImage*)getCatMapBgImageHighlighted:(BOOL)isHighlighted
{
    if (isHighlighted)
    {
        if (!catMapHighlightedBgImage)
            catMapHighlightedBgImage = [UIImage imageNamed:RW_CIMAP_BG_HL];
        return catMapHighlightedBgImage;
    }
    else {
        if (!catMapBackgroundImage)
            catMapBackgroundImage = [UIImage imageNamed:RW_CIMAP_BG];
        return catMapBackgroundImage;
    }
}
+(UIImage*)placeholderImageForIndustry:(RWIndustry*)industry
{
    return [UIImage imageNamed:[NSString stringWithFormat:RW_CPH_FORMAT,[RWIndustry getCategoryNameFromID:industry.ID]]];
}
//********************************************************************
#pragma mark - properties
-(NSString*)internalCategoryName
{
    return [RWIndustry getCategoryNameFromID:self.ID];
}
-(UIImage*)icon
{
    if (!cachedCategoryIcons)
        cachedCategoryIcons = [[NSMutableDictionary alloc] init];
    
    if (![cachedCategoryIcons objectForKey:self.name])
    {
        NSString *catImgFileName = [NSString stringWithFormat:RW_CI_NORMAL_FORMAT, self.internalCategoryName];
        UIImage *catIconImage = [UIImage imageNamed:catImgFileName];
        [cachedCategoryIcons setValue:catIconImage forKey:self.name];
    }
    
    return [cachedCategoryIcons objectForKey:self.name];

}

-(UIImage*)mapPinImage
{
    if (!cachedMapCategoryIcons)
        cachedMapCategoryIcons = [[NSMutableDictionary alloc] init];
    
    if (![cachedMapCategoryIcons objectForKey:self.name])
    {
        NSString *catImgFileName = [NSString stringWithFormat:RW_CIMAP_NORMAL_FORMAT, self.internalCategoryName];
        UIImage *catMapIconImage = [UIImage putImage:[UIImage imageNamed:catImgFileName]
                                             onImage:[RWIndustry getCatMapBgImageHighlighted:NO]
                                           withScale:RW_CIMAP_ICON_SCALE_FACTOR
                                     andCenterRatioX:RW_CIMAP_ICON_CENTERPOINT_X
                                                andY:RW_CIMAP_ICON_CENTERPOINT_Y];

        [cachedMapCategoryIcons setValue:catMapIconImage forKey:self.name];
    }
    return [cachedMapCategoryIcons objectForKey:self.name];
}

-(UIImage*)mapPinImageSelected
{
    if (!cachedMapCategoryIconsHl)
        cachedMapCategoryIconsHl = [[NSMutableDictionary alloc] init];
    

    if (![cachedMapCategoryIconsHl objectForKey:self.name])
    {
        NSString *catImgFileName = [NSString stringWithFormat:RW_CIMAP_HIGHLIGHTED_FORMAT, self.internalCategoryName];            
        UIImage *catMapIconImage = [UIImage putImage:[UIImage imageNamed:catImgFileName]                                                
                                             onImage:[RWIndustry getCatMapBgImageHighlighted:YES]
                                           withScale:RW_CIMAP_ICON_SCALE_FACTOR
                                     andCenterRatioX:RW_CIMAP_ICON_CENTERPOINT_X
                                                andY:RW_CIMAP_ICON_CENTERPOINT_Y];
        [cachedMapCategoryIconsHl setValue:catMapIconImage forKey:self.name];
    }          

    return [cachedMapCategoryIconsHl objectForKey:self.name];
}

-(UIImage*)placeholderImage
{
    return [RWIndustry placeholderImageForIndustry:self];
}
//********************************************************************
#pragma mark - public methods
// <#your public methods' code goes here#>

//********************************************************************
#pragma mark - private methods
// <#your private methods' code goes here#>

@end
