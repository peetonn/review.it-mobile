//
//  RWBaseObject.m
//  reviewit
//
//  Created by Peter Gusev on 8/5/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "RWBaseObject.h"

static RKRequestQueue *dataLoadingQueue = nil;

@interface RWBaseObject ()

+(RKObjectManager*)sharedManager;
+(RKRequestQueue*)dataLoadingQueue;

@end

@implementation RWBaseObject

@synthesize ID;
//********************************************************************
#pragma mark - received actions
// <#your IBAction's handlers goes here#>

//********************************************************************
#pragma mark - initialization and memory management
// <#your initialization&memory management code goes here#>

//********************************************************************
#pragma mark - delegation: <#sender#>
// <#your delegated methods goes here#>

//********************************************************************
#pragma mark - protocol: <#protocol name#>
// <#your conformed protocol code goes here#>

//********************************************************************
#pragma mark - notifications
// <#your notification's handlers' code goes here#>

//********************************************************************
#pragma mark - overriden
-(BOOL)isEqual:(id)object
{
    return [object isKindOfClass:[RWBaseObject class]] && (self.ID == [object ID]);
}

//********************************************************************
#pragma mark - class methods
+(RKObjectMapping*)objectMapping
{
    RKObjectMapping *baseObjectMapping = [RKObjectMapping mappingForClass:[RWBaseObject class]];
    [baseObjectMapping mapKeyPath:@"id" toAttribute:@"ID"];
    
    return baseObjectMapping;
}

+(void)loadObjectsWithMapping:(RKObjectMapping*)mapping onResourcePath:(NSString*)resourcePath withQueryParameters:(NSDictionary*)queryParams callback:(RWObjectArrayLoadingCallback)block
{
    NSString *resourcePathWithParams = resourcePath;

    if (queryParams)
        resourcePathWithParams = [resourcePath stringByAppendingQueryParameters:queryParams];

    RKObjectManager *manager = [RWBaseObject sharedManager];
    
    [manager loadObjectsAtResourcePath:resourcePathWithParams usingBlock:^(RKObjectLoader *loader){
        [loader setObjectMapping:mapping];
        loader.timeoutInterval = RW_REQUEST_TIMEOUT;
        loader.onDidFailLoadWithError = ^(NSError *error){
            block(nil,error);
        };
//        loader.onDidFailWithError =  ^(NSError *error){
//            block(nil,error);
//        };
        loader.onDidLoadObjects = ^(NSArray *objects){
            block(objects,nil);
        };
    }];
}

+(void)loadDataOnResourcePath:(NSString *)resourcePath withQueryParameters:(NSDictionary *)queryParams callback:(RWRawDataLoadingCallback)block
{
    NSString *resourcePathWithParams = resourcePath;

    if (queryParams)
        resourcePathWithParams = [resourcePath stringByAppendingQueryParameters:queryParams];

    RKRequest *request = [RKRequest requestWithURL:[NSURL URLWithString:resourcePathWithParams]];
    
    request.onDidLoadResponse = ^(RKResponse *response){
        block(response.body,nil);
    };
    request.onDidFailLoadWithError = ^(NSError *error){
        block(nil, error);
    };
    [[RWBaseObject dataLoadingQueue] addRequest:request];
}
//********************************************************************
#pragma mark - properties
// <#your customized accessors' code goes here#>

//********************************************************************
#pragma mark - public methods
// <#your public methods' code goes here#>

//********************************************************************
#pragma mark - private methods
+(RKObjectManager*)sharedManager
{
    RKObjectManager *manager = [RKObjectManager sharedManager];
    
    if (!manager)
    {
        manager = [RKObjectManager objectManagerWithBaseURLString:[[RWStorage sharedStorageController] getBaseURLStringForCurrentAPI]];
        [RKObjectManager setSharedManager:manager];
    }
    
    return manager;
}

+(RKRequestQueue*)dataLoadingQueue
{
    if (!dataLoadingQueue)
    {
        dataLoadingQueue = [RKRequestQueue requestQueueWithName:@"dataLoadingQueue"];
        [dataLoadingQueue start];
    }
    return dataLoadingQueue;
}

@end
