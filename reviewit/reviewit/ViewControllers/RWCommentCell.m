//
//  RWCommentCell.m
//  reviewit
//
//  Created by Peter Gusev on 10/21/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "RWCommentCell.h"
#import "PTNArtifacts/PTNLogger.h"

const CGRect messageLabelDefaultRect = {{10,5},{300,55}};
//const CGRect infoAreaDefaultRect = {{0,60},{320,40}};
const CGRect commentViewDefaultRect = {{0,0},{320,101}};

@implementation RWCommentCell

//********************************************************************
#pragma mark - received actions
// <#your IBAction's handlers goes here#>

//********************************************************************
#pragma mark - initialization and memory management
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

//********************************************************************
#pragma mark - delegation: RWCommentView
-(void)commentView:(RWCommentView *)commentView messageExpandedWithAnimationDuration:(NSTimeInterval)duration forNewSize:(CGSize)newSize
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(commentCell:willChangeSize:)])
        [self.delegate commentCell:self willChangeSize:newSize];
}
-(void)commentViewDidFinishedMessageExpansion:(RWCommentView *)commentView
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(commentCellDidChangeSize:)])
        [self.delegate commentCellDidChangeSize:self];
}
//********************************************************************
#pragma mark - protocol: <#protocol name#>
// <#your conformed protocol code goes here#>

//********************************************************************
#pragma mark - notifications
// <#your notification's handlers' code goes here#>

//********************************************************************
#pragma mark - overriden
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
//********************************************************************
#pragma mark - class methods
// <#your class methods code goes here#>

//********************************************************************
#pragma mark - properties
-(void)setReview:(RWComment*)review
{
    _review = review;
    
    self.commentView.messageLabel.isExpandable = !_review.wasViewed;
    self.commentView.messageLabel.text = _review.message;
    self.commentView.userLogoImageView.image = _review.user.image;
    self.commentView.userNameLabel.text = _review.user.name;
    self.commentView.dateLabel.text = _review.createdString;
}
-(RWComment*)review
{
    return _review;
}

//********************************************************************
#pragma mark - public methods

//********************************************************************
#pragma mark - private methods

@end
