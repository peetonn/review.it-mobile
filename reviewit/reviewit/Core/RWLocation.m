//
//  RWLocation.m
//  reviewit
//
//  Created by Peter Gusev on 8/14/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "RWLocation.h"

const NSUInteger RWRecentLocationsNum = 7;

#define RW_LOCATION_KEY_COUNTRY     @"country"
#define RW_LOCATION_KEY_LOCALITY    @"locality"
#define RW_LOCATION_KEY_SUBLOCALITY @"sublocality"
#define RW_LOCATION_KEY_LATITUDE    @"latitude"
#define RW_LOCATION_KEY_LONGITUDE   @"longitude"

@interface RWLocation ()

@property (nonatomic) double latitude;
@property (nonatomic) double longitude;

@end

@implementation RWLocation
@synthesize country, locality, subLocality, placemark = _placemark;

//********************************************************************
#pragma mark - initialization and memory management
-(id)initWithDictionary:(NSDictionary *)dictionary
{
    if ((self = [super init]))
    {
        self.country = [dictionary objectForKey:RW_LOCATION_KEY_COUNTRY];
        self.locality = [dictionary objectForKey:RW_LOCATION_KEY_LOCALITY];
        self.subLocality = [dictionary objectForKey:RW_LOCATION_KEY_SUBLOCALITY];
        self.latitude = [[dictionary objectForKey:RW_LOCATION_KEY_LATITUDE] doubleValue];
        self.longitude = [[dictionary objectForKey:RW_LOCATION_KEY_LONGITUDE] doubleValue];
    }
    return self;
}
-(id)initWithPlacemark:(CLPlacemark *)placemark
{
    if ((self = [super init]))
    {
        _placemark = placemark;
        self.country = placemark.country;
        self.locality = placemark.locality;
        self.subLocality = placemark.subLocality;
    }
    return self;
}
//********************************************************************
#pragma mark - delegation: <#sender#>
// <#your delegated methods goes here#>

//********************************************************************
#pragma mark - protocol: <#protocol name#>
// <#your conformed protocol code goes here#>

//********************************************************************
#pragma mark - notifications
// <#your notification's handlers' code goes here#>

//********************************************************************
#pragma mark - overriden
-(BOOL)isEqual:(id)object
{
    if (![object isKindOfClass:[self class]])
        return NO;
    
    RWLocation *comparingObject = (RWLocation*)object;
    
    return [self.readableName isEqual:comparingObject.readableName];
} 
-(NSString*)description
{
    return [self readableName];
}

//********************************************************************
#pragma mark - class methods
+(RWLocation*)locationWithPlacemark:(CLPlacemark*)placemark
{
    return [[RWLocation alloc] initWithPlacemark:placemark];
}
+(RWLocation*)locationFromDictionary:(NSDictionary*)dictionary
{
    return [[RWLocation alloc] initWithDictionary:dictionary];
}

//********************************************************************
#pragma mark - properties
-(BOOL)isGeocodingNeeded
{
    return (_placemark == nil);
}
-(NSString*)readableName
{
    NSString *name = @"";
    
    if (self.subLocality)
        name = self.subLocality;
    
    if (self.locality)
    {
        if ([name length])
            name = [name stringByAppendingString:@", "];
        name = [name stringByAppendingString: self.locality];
    }
    
    if (self.country)
    {
        if ([name length])
            name = [name stringByAppendingString:@", "];
        name = [name stringByAppendingString:self.country];
    }
    
    return name;
}
-(NSDictionary*)dictionary
{
    NSMutableDictionary *d = [[NSMutableDictionary alloc] init];

    if (self.country)
        [d setObject:self.country forKey:RW_LOCATION_KEY_COUNTRY];
    if (self.locality)
        [d setObject:self.locality forKey: RW_LOCATION_KEY_LOCALITY];
    if (self.subLocality)
        [d setObject:self.subLocality forKey: RW_LOCATION_KEY_SUBLOCALITY];
    
    [d setObject:[NSNumber numberWithDouble:self.location.coordinate.latitude] forKey:RW_LOCATION_KEY_LATITUDE];
    [d setObject:[NSNumber numberWithDouble:self.location.coordinate.longitude] forKey:RW_LOCATION_KEY_LONGITUDE];
    
    return d;
}
-(CLLocation*)location
{
    if (!self.placemark)
        return [[CLLocation alloc] initWithLatitude:self.latitude longitude:self.longitude];

    return self.placemark.location;
}
//********************************************************************
#pragma mark - public methods
-(NSString*)getShortName
{
    if (self.subLocality)
        return self.subLocality;
    if (self.locality)
        return self.locality;
    return self.country;
}
-(void)geocodeLocationWithCallback:(void(^)(RWLocation *location, NSError *err))block;
{
    if (!_placemark)
    {
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [geocoder geocodeAddressString:self.readableName completionHandler:^(NSArray *placemarks, NSError *err){
            if (!err)
            {
                _placemark = [placemarks objectAtIndex:0];
                block(self,nil);
            }
            else
                block(self,err);
        }];
    }
    else
        block(self,nil);
    
}
//********************************************************************
#pragma mark - private methods
// <#your private methods' code goes here#>

@end
