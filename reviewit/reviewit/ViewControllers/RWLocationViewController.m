//
//  RWLocationViewController.m
//  reviewit
//
//  Created by Peter Gusev on 7/13/12.
//  Copyright (c) 2012 peetonn inc. All rights reserved.
//

#import "PTNArtifacts/PTNLogger.h"
#import "RWLocationViewController.h"
#import <CoreLocation/CoreLocation.h>
#import "RWStorage.h"
#import "RWSimpleCell.h"

@interface RWLocationViewController ()
@end

@implementation RWLocationViewController
@synthesize searchBar;
@synthesize searchDisplayController;
@synthesize tableViewHeader;

@synthesize selectedLocation, delegate;

//********************************************************************
#pragma mark - received actions
- (IBAction)cancel:(id)sender {
    if (delegate && [delegate respondsToSelector:@selector(locationViewController:didChoseLocation:)])
        [delegate locationViewController:self didChoseLocation:nil];
}

- (IBAction)done:(id)sender {
    [[RWStorage sharedStorageController] addRecentLocation:self.selectedLocation];
    
    if (delegate && [delegate respondsToSelector:@selector(locationViewController:didChoseLocation:)])
        [delegate locationViewController:self didChoseLocation:self.selectedLocation];
}

//********************************************************************
#pragma mark - initialization and memory management
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {    
    }
    return self;
}

//********************************************************************
#pragma mark - view lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
 
    UIView *bgView =  [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"loctable_bg.png"]];
    bgView.frame = self.tableView.bounds;
    self.tableView.backgroundView = bgView;
    self.searchDisplayController.searchResultsTableView.backgroundView = bgView;
        
    self.tableViewHeader = [[[NSBundle mainBundle] loadNibNamed:@"RWSimpleTableViewHeader" owner:self options:nil] objectAtIndex:0];    
    self.tableViewHeader.titleLabel.text = @"Recent";
    
    recentPlaces = [[RWStorage sharedStorageController] getRecentLocations];
    searchResults = [[NSMutableArray alloc] init];
    geocoder = [[CLGeocoder alloc] init]; 
    self.searchBar.showsScopeBar = NO;
}

- (void)viewDidUnload
{
    recentPlaces = nil;
    searchResults = nil;
    geocoder = nil;
    
    [self setSearchDisplayController:nil];
    [self setSearchBar:nil];
    [self setSearchBar:nil];
    [self setTableViewHeader:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

//********************************************************************
#pragma mark - Table view data source
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ([tableView isEqual:self.tableView])
        return 20;
    return 0;
}    
-(UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if ([tableView isEqual:self.tableView])
        return self.tableViewHeader;
    return  nil;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([tableView isEqual:self.searchDisplayController.searchResultsTableView])
        return ([geocoder isGeocoding])?3:[searchResults count];
    else
        return [recentPlaces count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // for both search table view and table view dequeue cells from table view
    static NSString *CellIdentifier = @"RWCell";
    RWSimpleCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
        cell = [[RWSimpleCell alloc] init];
    
    if ([tableView isEqual:self.searchDisplayController.searchResultsTableView])
    {   
        if (geocoder.isGeocoding)
        {
            if (indexPath.row == 2)
                cell = [self.tableView dequeueReusableCellWithIdentifier:@"RWLoadingCell"];
            else 
                cell.textLabel.text = @"";            
        }
        else {      
            if ([searchResults count])
            {
                RWLocation *place = [searchResults objectAtIndex:indexPath.row];
                cell.textLabel.text = [place readableName];
            }
        }
    }
    else {
        RWLocation *place = [recentPlaces objectAtIndex:indexPath.row];
        cell.textLabel.text = [place readableName];
    }
    
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
 }   
 else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }   
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

//********************************************************************
#pragma mark - delegation - Table view
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([tableView isEqual: self.searchDisplayController.searchResultsTableView])
    {
        self.selectedLocation = [searchResults objectAtIndex:indexPath.row];
    }
    else {
        self.selectedLocation = [recentPlaces objectAtIndex:indexPath.row];
    }
    
    [self done:self];    
}



//********************************************************************
#pragma mark - delegation - UISearchDisplay delegate
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [searchResults removeAllObjects];
    
    if ([geocoder isGeocoding])
        [geocoder cancelGeocode];

    [geocoder geocodeAddressString:searchString completionHandler:^(NSArray* placemarks, NSError *error) {
        if (error)            
        {
            LOG_WARN(@"Error finding place: %@", error);
            [searchResults removeAllObjects];
        }
        else 
        {
            LOG_INFO(@"Found %d places", [placemarks count]);    
            [placemarks enumerateObjectsUsingBlock:^(CLPlacemark *place, NSUInteger idx, BOOL *stop){
                RWLocation *location = [RWLocation locationWithPlacemark:place];
                
                if (![searchResults containsObject:location])
                    [searchResults addObject: location];
            }];      
        }
        [self.searchDisplayController.searchResultsTableView reloadData];
    }];
    
    return YES;
}

//********************************************************************
#pragma mark - protocol: <#protocol name#>
// <#your conformed protocol code goes here#>

//********************************************************************
#pragma mark - notifications
// <#your notification's handlers' code goes here#>

//********************************************************************
#pragma mark - overriden
// <#your overide code goes here#>

//********************************************************************
#pragma mark - class methods
// <#your class methods code goes here#>

//********************************************************************
#pragma mark - properties    

//********************************************************************
#pragma mark - public methods
    

//********************************************************************
#pragma mark - private methods

@end
